"""
mship_cmd.py

Sample player script for commanding the player's Mothership. The Mothership
is the heart of the Player's Empire and contains some fuctionality not
seen in other ship scripts (like constructing other ships). This script is
very basic and only contains ship construction and undocking procedures.

"""

# This ship's data retains information between each execution of this script.
# data.initialized will be reset every time this ship changes scripts.
data = ship.getData()
if not data.initialized:

	# data.last_script is a tuple (script name, version) that will track
	# the previous script bound to this ship. It can be useful for managing
	# upgrades.
	if not data.last_script[0] == "mship_cmd":
		# Only initialize data when switching from another script.
		data.children = []
		data.enemies = []
		data.sex = 0
		ship.log("Initialized")
	else:
		# Do not remove anything above without writing an appropriate
		# conversation here
		ship.log("Upgraded")

	data.initialized = True

# Gaining access to the equipment opens up their API functionality.
comm = ship.getEquipment("comm")

# Motherships have the special capability of recieving messages from
# the client(and player) directly. This is the only direct influence
# the player will have over any of their ships/scripts.
# doShipCmd allows the player to control ship production.
def doShipCmd(cmd, args):
	if cmd == "create":
		# A Ship Factory is non-standard equipment only found
		# on certain class ships. The Mothership will always
		# have one by default.
		factory = ship.getEquipment("factory")
		new_id = factory.constructShip("ship_cmd")

		# Send the ship undocking information.
		comm.send(new_id, ("mship",))
		ship.log("Contructed new ship %d" % new_id)

	elif cmd == "sex":
		ship.log("Mucho sexo!")
		count = 5
		if args and len(args) == 1:
			count = int(args[0])
		data.sex = count


# Handle messages to the mothership
msgs = comm.queryMessages()
if msgs:
	for src, msg in msgs:
		if len(msg) < 2:
			continue
		elif msg[0] == "ship":
			doShipCmd(msg[1], msg[2:])

# If you are mature enough to stop giggling long enough to
# see the ship sexing just allows autocreating a certain amount
# of ships in sequence.
if data.sex > 0:
	ship.log("Autocreating ship %d" % data.sex)
	data.sex = data.sex - 1
	doShipCmd("create", None)
	