"""
ship_cmd.py

Sample player script for commanding the basic default ship. This starts
with proper undocking procedures when leaving the Mothership. Using the tracker
module's Hail functionality, it is capable of distinguishing friends from foe.
If a foe is detected and this Ship does not already have a valid target, it will
start to attack the foe. A more detailed description can be found through
inline documentation.

"""

# Ship scripts support importing modules provided by the pyshipcommand library,
# as well as Player Modules already uploaded to the server.
from pyshipcommand.target import TargetOrbit
from tracker import Hail

# Gaining access to the equipment opens up their API functionality.
comm = ship.getEquipment("comm")
weapon = ship.getEquipment("weapon")
radar = ship.getEquipment("radar")

# This ship's data retains information between each execution of this script.
# data.initialized will be reset every time this ship changes scripts.
data = ship.getData()
if not data.initialized:

	# data.last_script is a tuple (script name, version) that will track
	# the previous script bound to this ship. It can be useful for managing
	# upgrades.
	if not data.last_script[0] == "ship_cmd":
		# Only initialize data when switching from another script.
		data.mship_id = None
		data.mship_pos = None

		data.hailer = Hail()
		data.hailer.init(ship)

		ship.log("Initialized")
	else:
		# Do not remove anything above without writing an appropriate
		# conversation here.
		ship.log("Upgraded")

	data.initialized = True
# end initialization

# Use the communications array to retrieve any messages sent to this ship.
# Each message is a tuple (sender id, msg contents).
msgs = comm.queryMessages()
if msgs:
	for src, msg in msgs:
		# The ships log provides debugging capabilities that are sent to the 
		# client when it is connected.
		ship.log("[%d]:%s(%d)" % (src, str(msg), len(msg)))

		# Message contents can be in any format determined by the sender.
		# It is wise to use a shared common messaging format between all your ships,
		# otherwise they won't be able to communicate.
		if msg[0] == "mship":
			data.mship_id = src
			ship.log("Received Mothership Id %d" % src)
		elif msg[0] == "hail":
			data.hailer.handleHail(src, msg[1])

# Start performing tracking procedures after we recieve the undocking
# data from the Mothership.
if data.mship_id:
	# The radar can be used to detect nearby "Ships" or celestial bodies.
	# All detectable entities have a unique ID for easy tracking.
	entities = radar.detectEntities("Ship")
	for e in entities:
		if e.id == data.mship_id:
			# Store the Mothership location for future use, initiating
			# an orbit until further commands are recieved.
			if not data.mship_pos:
				ship.setTarget(TargetOrbit(e, 200))
				data.mship_pos = e.getPosition()
				ship.log("Began orbiting Mothership")
		else:
			# The hailer will start hailing procedures or just return
			# the status of known ships.
			h = data.hailer.hail(e.id)
			if not weapon.getTarget() and h == Hail.ENEMY:
				ship.log("Enemy[%d] detected. Attacking!" % e.id)
				weapon.setTarget(e.id)

				# Set new orbit around the target to stay in attack range.
				ship.setTarget(TargetOrbit(e, 100))

	# Keep attacking the target
	if weapon.getTarget():
		weapon.shoot()
