"""
mship_ai.py

Sample script for testing some more advanced AI logic as well as providing
the game with some AI opponents to test scripts on.

"""

from djlib.primitives import Circle
from math import pi
from random import shuffle

# This ship's data retains information between each execution of this script.
# data.initialized will be reset every time this ship changes scripts.
data = ship.getData()
if not data.initialized:

	# data.last_script is a tuple (script name, version) that will track
	# the previous script bound to this ship. It can be useful for managing
	# upgrades.
	if not data.last_script[0] == "mship_ai":
		# Only initialize data when switching from another script.
		data.MAX_ARMY = 10
		data.MAX_GUARDS = 4
		data.children = []
		data.idle_children = []

		data.known_enemies = {}
		data.known_res = {}

		data.future_tasks = {}
		data.current_tasks = {}

		# Represents 10 directions, 36 degrees apart.
		# Actual values represent distance explored
		data.explored_dirs = [0]*10

		ship.log("Initialized")
	else:
		# Do not remove anything above without writing an appropriate
		# conversation here
		ship.log("Upgraded")

	data.initialized = True

# Gaining access to the equipment opens up their API functionality.
comm = ship.getEquipment("comm")

# Just construct a new ship
def createShip():
	factory = ship.getEquipment("factory")
	new_id = factory.constructShip("ship_ai")
	data.children.append(new_id)

	# Send the ship undocking information.
	comm.send(new_id, ("mship", "id"))
	ship.log("Contructed new ship %d" % new_id)
#end createShip

# Find a task for a ship(c_id) to perform.
# 'task' can be used to further specify a specific type of task.
def assignTask(c_id, task = ""):

	if task == "":
		# Temporary priority list to decide what task to perform
		# if 'task' was not specified
		temp_priority = ["guard", "scout"]

		for t in temp_priority:
			if t in data.future_tasks:
				task = t
				break

	# Check for exist of all or any task
	tasks = data.future_tasks.get(task)
	if not tasks:
		return False

	# Retrieve task and send command to c_id
	t = tasks.pop()
	ship.log("[ASSIGN] Ship[%d] to %s: %s" % (c_id, task, str(t)))
	comm.send(c_id, ("mship", "task", task, t))

	if c_id in data.idle_children:
		data.idle_children.remove(c_id)

	# Add to current tasks
	current = data.current_tasks.get(task, [])
	current.append((c_id, t))
	data.current_tasks[task] = current

	# Clean up future tasks
	if not tasks:
		del data.future_tasks[task]

	return True
#end assignTask

# Perform some high level logic to generate current or future tasks
# to be performed by our ships. If ships are available, give them the orders.
def generateTasks():
	# Check Guards
	num_guards = len(data.current_tasks.get("guard", []))
	if num_guards < data.MAX_GUARDS:
		guards = data.future_tasks.get("guard", [])
		num_guards = num_guards + len(guards) #check _inplacevar_
		if num_guards < data.MAX_GUARDS:
			ship.log("[NEW TASK] %d MShip Guards needed" % (data.MAX_GUARDS - num_guards))
			guards.extend([ship.id]*(data.MAX_GUARDS  - num_guards))
			data.future_tasks["guard"] = guards

	# Check Scouts
	scouts = data.future_tasks.get("scout", [])
	if not scouts:
		min_d = min(data.explored_dirs)
		scout_dir = [i for i,d in enumerate(data.explored_dirs) if d == min_d]
		shuffle(scout_dir)

		dir_circle = Circle(ship.pos, min_d + 1000)
		rad_step = (2.0*pi)/len(data.explored_dirs)
		for i in scout_dir:
			d = dir_circle.pointOnCircle(i*rad_step)
			scouts.append(d)
			ship.log("[NEW TASK] Scout requested at %s (%d-%d)" % (str(d), i, min_d+1000))
			data.explored_dirs[i] = min_d + 1000
		data.future_tasks["scout"] = scouts

	# Automatically assign new tasks to idle children
	for c in data.idle_children:
		assignTask(c)
#end generateTasks

# Process commands sent by children ships
def doShipCmd(src_id, cmd, args):
	if src_id not in data.children:
		ship.log("Recieved Ship Command from Unknown Ship[%d]" % src_id)
		return

	if cmd == "idle":
	# A child ship has become idle, find something for him to do
		if not assignTask(src_id):
			data.idle_children.append(src_id)
		
	#end "idle"
	elif cmd == "enemy":
	# Process information about enemy sightings
		for e_id, e_pos in args:
			e = data.known_enemies.get(e_id)

			# need a ship.time() API so we can make sure sightings are the "newest"
			if e:
				ship.log("Updating known Enemy[%s] location %s" % (e_id, str(e_pos)))
			else:
				ship.log("New Enemy[%d] found at location %s" % (e_id, str(e_pos)))
			data.known_enemies[e_id] = e_pos
	#end "enemy"

#end doShipCommand

# Handle messages to the mothership
msgs = comm.queryMessages()
if msgs:
	for src, msg in msgs:
		if len(msg) < 2:
			continue
		elif msg[0] == "ship":
			doShipCmd(src, msg[1], msg[2:])


# Maintain a certain number of children for an army
if len(data.children) < data.MAX_ARMY:
	ship.log("Autocreating ship %d/%d" % (len(data.children), data.MAX_ARMY))
	createShip()

# Maintain a certain number of tasks
if not data.future_tasks:
	generateTasks()