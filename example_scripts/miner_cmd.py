"""
miner_cmd.py

Another sample player script to demonstrate of different script logic
can effect the behavior of a ship, even using similiar equipment.
Instead of searching for and attacking enemy ships, this script is
designed to look for celestial bodies and mine their precious resources.

"""

# Ship scripts support importing modules provided by the pyshipcommand library,
# as well as Player Modules already uploaded to the server.
from pyshipcommand.target import TargetOrbit
from tracker import Hail

# Gaining access to the equipment opens up their API functionality.
comm = ship.getEquipment("comm")
weapon = ship.getEquipment("weapon")
tool = ship.getEquipment("tool")
radar = ship.getEquipment("radar")

# This ship's data retains information between each execution of this script.
# data.initialized will be reset every time this ship changes scripts.
data = ship.getData()
if not data.initialized:

	# data.last_script is a tuple (script name, version) that will track
	# the previous script bound to this ship. It can be useful for managing
	# upgrades.
	if data.last_script[0] not in ["ship_cmd", "miner_cmd"]:
		data.mship_id = None
		data.mship_pos = None

		data.hailer = Hail()
		data.hailer.init(ship)

		ship.log("Initialized")
	else:
		# Do not remove anything above without writing an appropriate
		# conversation here
		ship.log("Upgraded")

	data.initialized = True
# end initialization

# Use the communications array to retrieve any messages sent to this ship.
# Each message is a tuple (sender id, msg contents).
msgs = comm.queryMessages()
if msgs:
	for src, msg in msgs:
		# The ships log provides debugging capabilities that are sent to the 
		# client when it is connected.
		ship.log("[%d]:%s(%d)" % (src, str(msg), len(msg)))

		# Message contents can be in any format determined by the sender.
		# It is wise to use a shared common messaging format between all your ships,
		# otherwise they won't be able to communicate.
		if msg[0] == "mship":
			data.mship_id = src
			ship.log("Received Mothership Id %d" % src)
		elif msg[0] == "hail":
			# Even minings have to handle hails to prevent from being attacked
			# by our own ships!
			data.hailer.handleHail(src, msg[1])

if data.mship_id:
	# The radar can be used to detect nearby "Ships" or celestial bodies.
	# All detectable entities have a unique ID for easy tracking.
	# Filters can be used so the radar only returns Entity Types that we
	# care about. In shis case, other Ships and Asteroids.
	entities = radar.detectEntities(["Ship", "Asteroid"])
	for e in entities:
		if e.id == data.mship_id:
			# Store the Mothership location for future use, initiating
			# an orbit until further commands are recieved.
			if not data.mship_pos:
				ship.setTarget(TargetOrbit(e, 200))
				data.mship_pos = e.getPosition()
				ship.log("Began orbiting Mothership")
		elif e.type == "Ship":
			h = data.hailer.hail(e.id)
		elif e.type == "Asteroid":
			# The default 'tool' on ships is a mining laser used to extract
			# valuable resources. We can lock onto targets, just like other weapons.
			if tool.getTarget() is None:
				ship.log("Asteroid at %s found. Mining..." % str(e.pos))
				tool.setTarget(e)
				ship.setTarget(TargetOrbit(e, e.radius + 50))

	# Continue to mine any valid target.
	if tool.getTarget():
		tool.mine()
