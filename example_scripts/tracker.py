"""
tracker.py

Sample player module for pyShipCommand that demonstrates how code can be
written and reused by multiple player scripts. This particular module
provides a sample 'Hailing' class that ships can use to identify whether
another ship is a friend or foe. There are obviously some flaws to this
method, and should only be used at the players discretion.

How the hailing system works:
1) Hail the other ship using the name of the ships owner.
2) Ship recieves hail and compares that ships owner with this ships.

* Mark as friend if owners match
* Mark as foe if ship owners do not match
* Ships that do not respond to hails after a certain number of tries
  are marked as foe.

"""


class Hail:
	UNKNOWN = 0
	FRIEND = 1
	ENEMY = 2

	MAX_HAILS = 4

	def init(self, ship):
		"""Needs to be called during the ship scripts intialization."""
		self.ship = ship
		self.friends = []
		self.enemies = []
		self.hail_count = {}

	def hail(self, ship_id):
		"""
		Attempt to identify another ship based on the ship id.
		No message is sent if the ship is already a known friend/foe
		"""
		# check if this already exists
		if ship_id in self.friends:
			return Hail.FRIEND
		elif ship_id in self.enemies:
			return Hail.ENEMY

		# Increment hail count and condemn any ship that doesn't rehail in MAX_HAILS
		h = self.hail_count.get(ship_id, 0) + 1
		if h > Hail.MAX_HAILS:
			#self.ship.log("Hail to %d timed out." % ship_id)
			self.enemies.append(ship_id)
			del self.hail_count[ship_id]
			return Hail.ENEMY

		self.hail_count[ship_id] = h
		#self.ship.log("Hailing %d for the %d time" % (ship_id, h))
		comm = self.ship.getEquipment("comm")
		comm.send(ship_id, ("hail", self.ship.owner))
		return Hail.UNKNOWN

	
	def handleHail(self, src_id, owner):
		"""
		Handle a hailing message sent by another instance of this class.
		This method contains the logic that determines what classifies
		friend from foe.
		"""
		#self.ship.log("Handling hail from %d" % src_id)
		# check if hail is already decided
		if src_id in self.friends:
			# Do not respond to hails from friends, since we already hailed them
			return Hail.FRIEND
		elif src_id in self.enemies:
			# Let the suckers wait
			return Hail.ENEMY

		#self.ship.log("Hail from %s" % owner)
		if owner == self.ship.owner:
			# Only re-hail someone we haven't hailed before
			if not self.hail_count.get(src_id):
				#self.ship.log("Responding to Hail from %d" % src_id)
				self.hail(src_id)

			self.friends.append(src_id)
			return Hail.FRIEND
		return Hail.ENEMY
#end Hail