"""
ship_ai.py

Sample script for providing some ship-level AI as well as organized communication
with other AI entities. This will also be an example of "task" based processing.

"""

# Ship scripts support importing modules provided by the pyshipcommand library,
# as well as Player Modules already uploaded to the server.
from pyshipcommand.target import TargetOrbit, TargetPosition
from tracker import Hail

# Gaining access to the equipment opens up their API functionality.
comm = ship.getEquipment("comm")
weapon = ship.getEquipment("weapon")
radar = ship.getEquipment("radar")

# This ship's data retains information between each execution of this script.
# data.initialized will be reset every time this ship changes scripts.
data = ship.getData()
if not data.initialized:

	# data.last_script is a tuple (script name, version) that will track
	# the previous script bound to this ship. It can be useful for managing
	# upgrades.
	if not data.last_script[0] == "ship_ai":
		# Only initialize data when switching from another script.
		data.mship_id = None
		data.mship_pos = None

		data.hailer = Hail()
		data.hailer.init(ship)

		data.task = None
		data.enemies = {}
		data.mship_enemies = []

		ship.log("Initialized")
	else:
		# Do not remove anything above without writing an appropriate
		# conversation here.
		ship.log("Upgraded")

	data.initialized = True
# end initialization

def changeTask(new_task):
	if new_task == data.task:
		ship.log("DEBUG: Invalid state change %s -> %s" % (data.task, new_task))
		return

	# Handle remaining tasks that have no source req
	if new_task == "idle":
		ship.log("Began orbiting Mothership")
		ship.setTarget(TargetOrbit(TargetPosition(data.mship_pos), 200))
		comm.send(data.mship_id, ("ship", "idle"))
	elif new_task == "report":
		ship.log("Reporting back to Mothership")
		ship.setTarget(TargetOrbit(TargetPosition(data.mship_pos), 200))

	ship.log("Ship[%d] task changed %s -> %s" % (ship.id, data.task, new_task))
	data.task = new_task
#end changeTask

def doMshipCmd(src_id, cmd, args):

	# mship "id" cmd can only be processed without an existing mship id
	if not data.mship_id:
		if cmd == "id":
			data.mship_id = src_id
			ship.log("Received Mothership Id %d" % src)
		return

	# check for valid mship src
	if not src_id == data.mship_id:
		ship.log("Recieved MShip Command from Unknown Ship[%d]" % src_id)

	if cmd == "task":
		ship.log("[TASK] %s" % str(args))
		if len(args) < 2:
			return

		task = args[0]
		if task == "guard":
			if not args[1] == data.mship_id:
				return
			ship.setTarget(TargetOrbit(TargetPosition(data.mship_pos), 200))
		elif task == "scout":
			ship.setTarget(TargetPosition(args[1]))

		changeTask(args[0])
#end doMshipCmd

def processEntities(entities):
	for e in entities:
		if e.id == data.mship_id:
			# Store the Mothership location for future use, initiating
			# an orbit until further commands are recieved.
			if not data.mship_pos:
				data.mship_pos = e.getPosition()
				changeTask("idle")
			elif data.task == "report":
				# Report to mothership once in range
				if len(data.enemies):
					comm.send(data.mship_id, ("enemy", data.enemies.items()))
					data.mship_enemies.append(data.enemies.keys())
					data.enemies = {}
				changeTask("idle")
		else:
			# The hailer will start hailing procedures or just return
			# the status of known ships.
			h = data.hailer.hail(e.id)
			if h == Hail.ENEMY:
				# Ignore new enemies if we already have a target
				if weapon.getTarget():
					return

				# If MShip already knows about this enemy, attack it!
				if e.id in data.mship_enemies:
					ship.log("Known Enemy[%d] detected. Attacking!" % e.id)
					ship.setTarget(TargetOrbit(e, 100))
					weapon.setTarget(e.id)
					return

				data.enemies[e.id] = e.pos
				if data.task == "scout":
					changeTask("report")
#end processEntities

# Use the communications array to retrieve any messages sent to this ship.
# Each message is a tuple (sender id, msg contents).
msgs = comm.queryMessages()
if msgs:
	for src, msg in msgs:
		# The ships log provides debugging capabilities that are sent to the 
		# client when it is connected.
		#ship.log("[%d]:%s(%d)" % (src, str(msg), len(msg)))

		# Message contents can be in any format determined by the sender.
		# It is wise to use a shared common messaging format between all your ships,
		# otherwise they won't be able to communicate.
		if msg[0] == "mship":
			doMshipCmd(src, msg[1], msg[2:] if len(msg) > 2 else None)
		elif msg[0] == "hail":
			data.hailer.handleHail(src, msg[1])

# Start performing tracking procedures after we recieve the undocking
# data from the Mothership.
if data.mship_id:
	# The radar can be used to detect nearby "Ships" or celestial bodies.
	# All detectable entities have a unique ID for easy tracking.
	processEntities(radar.detectEntities("Ship"))

	# task related updates
	if data.task == "scout":
		if ship.atTarget():
			ship.log("Scout target reached!")
			changeTask("report")

	# Keep attacking the target
	if weapon.getTarget():
		weapon.shoot()
