
# SYSTEM

# ADDITIONAL LIBRARIES
from twisted.spread import pb

# LOCAL
from states.loading import LoadingState
from states.viewer import ViewerState
from cscripts import ClientScriptMgr
from pyshipcommand.network import PBClient, NetVector, NetShip, NetInit, NetScript, NetCelestialBody

from djlib.logger import getLogger
log = getLogger('pyshipcommand.gui.client')

class BaseClient(PBClient):
    def __init__(self):
        PBClient.__init__(self)

    def remote_print(self, msg):
        print msg
#end BaseClient

class RegisterClient(BaseClient):

    def login(self, username, password):
        self.username = username
        self.password = password
        BaseClient.login(self, "register", "account")

    def connected(self):
        dfr = self.server.callRemote("registerAccount", self.username, self.password)
        dfr.addCallback(self.disconnect)

    def disconnect(self, n):
        BaseClient.disconnect(self)
#end RegisterClient

pb.setUnjellyableForClass(NetShip, NetShip)
pb.setUnjellyableForClass(NetInit, NetInit)
pb.setUnjellyableForClass(NetScript, NetScript)
pb.setUnjellyableForClass(NetCelestialBody, NetCelestialBody)
class GUIClient(BaseClient, pb.Referenceable):
    
    def __init__(self, gameclass):
        BaseClient.__init__(self)
        self.REF = self
        self.gc = gameclass

        self.scripts = ClientScriptMgr()

    def connected(self):
        self.gc.changeState(LoadingState)

    def exit(self):
        self.gc.quit()

    # SERVER COMMUNICATION
    def info(self):
        self.server.callRemote("info")

    def giveScript(self, name, source):
        self.server.callRemote("giveScript", name, source)

    def addShip(self, pos):
        self.server.callRemote("addShip", NetVector.fromVector(pos))

    def moveShip(self, ship_id, target_pos):
        self.server.callRemote("moveShip", ship_id, NetVector.fromVector(target_pos))

    def orbitShip(self, ship_id, target_pos, radius):
        self.server.callRemote("orbitShip", ship_id, NetVector.fromVector(target_pos), radius)

    def followShip(self, ship_id, target_id, dist):
        self.server.callRemote("followShip", ship_id, target_id, dist)

    def scriptShip(self, ship_id, script_name, action):
        self.server.callRemote("scriptShip", ship_id, script_name, action)

    def giveMessage(self, msg):
        self.server.callRemote("giveMessage", msg)

    def giveChat(self, msg):
        self.server.callRemote("giveChat", msg)

    def getPlayers(self):
        self.server.callRemote("getPlayers")

    def getClientInit(self, cb):
        dfr = self.server.callRemote("getClientInit")
        dfr.addCallback(self.setClientInit)
        dfr.addCallback(cb)

    # CLIENT CALLBACKS

    def remote_updateShips(self, net_ships):
        # Sanity check
        if not net_ships:
            return
        log.debug("Received %d ship updates!", len(net_ships))

        # We only use ship updates when we are in the ViewerState
        state = self.gc.getActiveState()
        if isinstance(state, ViewerState):
            state.updateShips(net_ships)

    def remote_log(self, log):
        for l in log:
            print l

    def remote_showText(self, text):
        # We only send text when in ViewerState
        state = self.gc.getActiveState()
        if isinstance(state, ViewerState):
            state.delayedText(text, 5000)

    def setClientInit(self, init_data):
        # Load player scripts
        self.scripts.setActivePlayer(init_data.player)
        log.info("Scripts: %s", self.scripts.getScriptList())

        # Cache current server scripts for player
        self.scripts.setServerScripts(init_data.scripts)
        return init_data

#end GUIClient