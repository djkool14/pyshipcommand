
# ADDITIONAL LIBRARIES
import pygame

# LOCAL
from djlib.primitives import Vector, Rect
from djlib.spatial import ExpandingRectTree
from djlib.logger import getLogger
log = getLogger('pyshipcommand.gui.viewer')

class ShipViewer:
    
    # CONSTANTS
    SHIP_RADIUS = 30
    
    # ASSETS
    SHIP_IMG = None
    MSHIP_IMG = None
    FONT = None

    FONT_COLOR = pygame.Color(0, 255, 0)
    TREE_COLOR = pygame.Color(128, 128, 128)

    def __init__(self, rs):
        self.ships = {}
        self.mship = None
        self.destroyed_ships = []
        
        self.lerp_time = 0
        self.server_step = 0
        self.player_id = 0

        (w,h) = rs.get_size()

        #ExpandingRectTree.MAX_DATA = 2
        self.tree = ExpandingRectTree(Rect.fromSides(0, 0, w, h))
        self.view_node = None

    def setClientInit(self, init_data):

        log.info("ClientInit Received with Player ID %d", self.player_id)
        log.info("Server Step: %f", init_data.server_step)
        log.info("View Position: %s", str(init_data.start_pos))

        self.player_id = init_data.player_id
        self.setServerStep(init_data.server_step)

        self.updateShips(init_data.ships)
        self.mship = self.ships[init_data.mship_id]
        
        
    def updateShips(self, net_ships):

        # delete any ships not updated last frame
        log.debug(self.destroyed_ships)
        for ship_id in self.destroyed_ships:
            self.tree.remove(self.ships[ship_id])
            del self.ships[ship_id]

        self.destroyed_ships = self.ships.keys()
        log.debug(self.destroyed_ships)

        for nship in net_ships:
            ship = self.ships.get(nship.id)
            if ship:
                self.tree.remove(ship)
                ship.update(nship)
                self.tree.insert(ship)

                self.destroyed_ships.remove(nship.id)
            else:
                self.ships[nship.id] = nship
                self.tree.insert(nship)
                
        # reset interpolation time
        self.lerp_time = 0.0

    def update(self, dt):
        # Interpolate ship positions between server updates
        self.lerp_time += dt
        int_time = self.lerp_time/self.server_step
        for ship in self.ships.itervalues():
            ship.interpolate(int_time)
   
    def draw(self, rs, view_rect = None):
        pos = -view_rect.pos

        # Debug render of spatial tree
        new_node = self.tree.minNode(view_rect)
        if not new_node:
            new_node = self.tree
        if new_node != self.view_node:
            log.debug("New view node has %d ships.", new_node.count())
            self.view_node = new_node
        self.drawTree(rs, self.view_node, pos)

        # Draw Ships
        offset = None
        ship_pos = None
        for ship in self.view_node:

            if ship.id in self.destroyed_ships:
                continue # skip destroyed ships

            rotImg = None
            if ship.mship:
                rotImg = self.MSHIP_IMG
            else:
                rotImg = pygame.transform.rotate(self.SHIP_IMG, ship.rotation)
            
            offset = Vector(rotImg.get_width()/-2, rotImg.get_height()/-2)
            ship_pos = pos + ship.pos + offset
            rs.blit(rotImg, ship_pos.intArgs())

            # draw id above ship
            self.renderText(rs, (pos + ship.pos + Vector(0, -20)).intArgs(), str(ship.id), True)

            # draw recent attacks
            if ship.recent_attack and ship.weapon_a > 0.0:
                target_ship = self.ships.get(ship.recent_attack)
                if target_ship:
                    wep_color = pygame.Color(255, 0, 0, int(ship.weapon_a*255))
                    pygame.draw.line(rs, wep_color, (pos+ship.pos).intArgs(), (pos+target_ship.pos).intArgs(), 2)

    def drawTree(self, rs, node, pos):
        true_pos = pos + node.rect.pos
        rect = pygame.Rect(true_pos.intArgs(), node.rect.size.intArgs())
        pygame.draw.rect(rs, self.TREE_COLOR, rect, 2)
        self.renderText(rs, true_pos.intArgs(), str(len(node.data) if node.data else 0))
        if node.children:
            for child in node.children:
                self.drawTree(rs, child, pos)

    def renderText(self, rs, pos, text, centered = False):
        surf = self.FONT.render(text, False, self.FONT_COLOR)
        if centered:
            pos = (pos[0]-surf.get_width()/2, pos[1]-surf.get_height()/2)
        rs.blit(surf, pos)

    def setServerStep(self, dt):
        log.info("Server time step set to %f seconds", dt)
        self.server_step = dt

    def pickShip(self, real_pos):
        # Compare mouse pos against all visible ships
        close_ship = None
        close_dist = self.SHIP_RADIUS
        dist = None
        for ship in self.view_node:
            dist = real_pos.distanceApart(ship.pos)
            if dist < close_dist:
                close_ship = ship
                close_dist = dist

        # pick closest ship within click range
        return close_ship

#end ShipViewer

class Sector:
    def __init__(self, pos, stars):
        self.pos = pos
        self.stars = stars
#end Sector

import random
class StarGenerator:

    STAR_COLOR = pygame.Color(255, 255, 255)
    STARS_MAX_SIZE = 3

    from djlib.primitives import Circle as Star

    def __init__(self, sector_size, screen_size, star_freq):
            self.sector_size = sector_size
            self.star_freq = star_freq

            # Round up to guarentee overlap
            self.repeat = ((screen_size[0]/sector_size[0]) + 1, (screen_size[1]/sector_size[1]) + 1)
            star_size = Vector(self.repeat[0]*sector_size[0], self.repeat[1]*sector_size[1])
            star_pos = Vector((star_size[0]-screen_size[0])/-2, (star_size[1]-screen_size[1])/-2)

            self.star_bounds = Rect(star_pos, star_size)
            log.debug(self.star_bounds)
            
            # create initial sectors
            self.sectors = []
            for y in xrange(self.repeat[1]):
                for x in xrange(self.repeat[0]):
                    self.sectors.append(self._generateSector(star_pos + Vector(x*sector_size[0], y*sector_size[1])))
    
    def setView(self, view_rect, full_gen = False):
            if self.star_bounds.contains(view_rect):
                return

            view_corner = view_rect.pos + view_rect.size
            star_corner = self.star_bounds.pos + self.star_bounds.size

            sect_count = len(self.sectors)

            if not full_gen:
                # Gen Left
                if view_rect.pos[0] < self.star_bounds.pos[0]:
                    self.star_bounds.move(Vector(-self.sector_size[0], 0))
                    for y in range(self.repeat[1]):
                        self.sectors.append(self._generateSector(self.star_bounds.pos + Vector(0, y*self.sector_size[1])))
                    log.debug("Gen Left")

                # Gen Right
                elif view_corner[0] > star_corner[0]:
                    self.star_bounds.move(Vector(self.sector_size[0], 0))
                    start = Vector(star_corner[0], self.star_bounds.pos[1])
                    for y in range(self.repeat[1]):
                        self.sectors.append(self._generateSector(start + Vector(0, y*self.sector_size[1])))
                    log.debug("Gen Right")

                # Gen Up
                if view_rect.pos[1] < self.star_bounds.pos[1]:
                    self.star_bounds.move(Vector(0, -self.sector_size[1]))
                    for x in range(self.repeat[0]):
                        self.sectors.append(self._generateSector(self.star_bounds.pos + Vector(x*self.sector_size[0], 0)))
                    log.debug("Gen Up")

                # Gen Down
                elif view_corner[1] > star_corner[1]:
                    self.star_bounds.move(Vector(0, self.sector_size[1]))
                    start = Vector(self.star_bounds.pos[0], star_corner[1])
                    for x in range(self.repeat[0]):
                        self.sectors.append(self._generateSector(start + Vector(x*self.sector_size[0], 0)))
                    log.debug("Gen Down")

            else: #full_gen
                # create initial sectors
                self.sectors = []
                log.debug(self.star_bounds)
                star_overlap = Vector((self.star_bounds.size[0]-view_rect.size[0])/-2,
                                      (self.star_bounds.size[1]-view_rect.size[1])/-2)
                self.star_bounds.setPosition(view_rect.pos + star_overlap)
                for y in xrange(self.repeat[1]):
                    for x in xrange(self.repeat[0]):
                        self.sectors.append(self._generateSector(
                            self.star_bounds.pos + Vector(x*self.sector_size[0], y*self.sector_size[1])))

                log.debug("Full Generation of %d sectors", len(self.sectors))
                log.debug(self.star_bounds)
            #end if

            log.debug("Generated %d sectors", len(self.sectors) - sect_count)
            sect_count = len(self.sectors)

            # Delete any sectors that are no longer visible
            self.sectors = [sector for sector in self.sectors if self.star_bounds.contains(sector.pos)]
            log.debug("Deleted %d sectors", sect_count - len(self.sectors))

    def draw(self, rs, view_rect = None):
        pos = Vector(0,0)
        if view_rect:
            pos = view_rect.pos

        star_pos = None
        for sector in self.sectors:
            for star in sector.stars:
                star_pos = star.pos - pos
                pygame.draw.circle(rs, self.STAR_COLOR, star_pos.intArgs(), star.radius)
        
    def _generateSector(self, sector_pos):
            
            # seed random with the sector_pos so starts will always look the same in that sector
            state = random.getstate()
            random.seed(hash(sector_pos))
            
            stars = []
            vec = None
            for i in xrange(self.star_freq):
                vec = Vector(random.randint(sector_pos[0], sector_pos[0]+self.sector_size[0]), 
                             random.randint(sector_pos[1], sector_pos[1]+self.sector_size[1]))
                stars.append(self.Star(vec, random.randint(1, self.STARS_MAX_SIZE)))        
            
            # restore random state
            random.setstate(state)
            
            return Sector(sector_pos, stars)

#end StarGenerator
