
# SYSTEM
from math import pi
from pkg_resources import resource_stream

# ADDITIONAL LIBRARIES
import pygame

# LOCAL
from pyshipcommand_gui.dialogs import LoginDialog
from pyshipcommand_gui.client import RegisterClient

from djlib.game import GameState
from djlib.primitives import Circle, Point, Rect
from djlib.logger import getLogger
log = getLogger('pyshipcommand.gui.state.login')

# CONSTANTS
TITLE_POS = (100, 50)
ORBIT_RADIUS = 500
ORBIT_SPEED = 0.04

class LoginState(GameState):

    def __init__(self):
        GameState.__init__(self)

        # Itemize LoginState variables for reference
        self.titleImg = None
        self.loginWnd = None
        self.star_period = 0.0

    def initialize(self):
        """Called the first time the game is changed to this state
           during the applications lifecycle."""
        rs = resource_stream('pyshipcommand_gui', 'assets/pyship_logo.png')
        self.titleImg = pygame.image.load(rs)

        # Create Login Dialog
        (w,h) = self.gc.rsurf.get_size()
        self.loginWnd = LoginDialog(self.gc.client, RegisterClient(), w, h)
        self.loginWnd.populateUser(self.gc.DEFAULT_USER)
        self.loginWnd.populateServer(self.gc.DEFAULT_SERVER, self.gc.DEFAULT_PORT)

    def enter(self):
        """Called every time the game is switched to this state."""
        self.gc.uirender.add_widget(self.loginWnd)
        #self.loginWnd.resetPass()
        self.loginWnd.set_focus()

    def processInput(self):
        """Called during normal update/render period for this state
           to process it's input."""

    def update(self):
        """Called during normal update/render period for this state
           to update it's local or game data."""
        self.star_period += ORBIT_SPEED * self.gc.time_step
        if self.star_period > (2.0*pi):
            self.star_period = self.star_period - (2.0*pi)

    def render(self):
        """Called during normal update/render period for this state
           to render it's data in a specific way."""
        # get render surface
        rs = self.gc.rsurf
        rs.fill(self.gc.COLOR_BLACK)

        # update stars
        star_orbit = Circle(Point(0,0), ORBIT_RADIUS)
        star_pos = star_orbit.pointOnCircle(self.star_period)
        star_pos = Point(int(star_pos[0]), int(star_pos[1]))
        star_view = Rect.fromPointSize(star_pos,
                                       *self.gc.rsurf.get_size())
        #print str(star_view)

        self.gc.stars.setView(star_view)
        self.gc.stars.draw(rs, star_view)

        # Draw title image if present
        if self.loginWnd:
            rs.blit(self.titleImg, TITLE_POS)

        # Now apply UI
        #g_uirender.update()
        rs.blit(self.gc.uirender.screen, (0,0), None, pygame.BLEND_ADD)

        pygame.display.update()

    def leave(self):
        """Called whenever we switch from this state to another."""
        self.gc.uirender.remove_widget(self.loginWnd)

    def shutdown(self):
        """Called during application shutdown."""
        self.loginWnd.destroy()

#end LoginState

