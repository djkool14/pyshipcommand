
# SYSTEM
from random import random

# ADDITIONAL LIBRARIES
import pygame

# LOCAL

from djlib.game import GameState
from djlib.primitives import Vector
from djlib.logger import getLogger
log = getLogger('pyshipcommand.gui.state.loading')

from viewer import ViewerState

# CONSTANTS
MIN_LOADING = 1.0
STAR_SPEED = 1.0
STAR_BLUR = 5

# TestState
class LoadingState(GameState):

    def __init__(self):
        GameState.__init__(self)

        # Itemize LoginState variables for reference

    def initialize(self):
        """Called the first time the game is changed to this state
           during the applications lifecycle."""

    def enter(self):
        """Called every time the game is switched to this state."""
        self.gc.client.getClientInit(self.setClientInit)

    def processInput(self):
        """Called during normal update/render period for this state
           to process it's input."""

    def update(self):
        """Called during normal update/render period for this state
           to update it's local or game data."""

    def render(self):
        """Called during normal update/render period for this state
           to render it's data in a specific way."""
        # get render surface
        rs = self.gc.rsurf
        rs.fill(self.gc.STAR_COLOR)

        # Now apply UI
        #rs.blit(self.gc.uirender.screen, (0,0), None, pygame.BLEND_ADD)

        pygame.display.update()

    def leave(self):
        """Called whenever we switch from this state to another."""

    def shutdown(self):
        """Called during application shutdown."""

    def setClientInit(self, client_init):
        self.changeState(ViewerState, client_init)


#end TestState

