
# SYSTEM
from pkg_resources import resource_stream
from os import path
from glob import glob

# ADDITIONAL LIBRARIES
import pygame
from pygame import K_UP, K_DOWN, K_LEFT, K_RIGHT, K_RETURN, K_SLASH

from ocempgui import widgets
from ocempgui.widgets.Constants import SIG_INPUT, SIG_KEYDOWN, SIG_MOUSEDOWN, SIG_MOUSEMOVE
from ocempgui.events import INotifyable

# LOCAL
from djlib.game import GameState
from djlib.primitives import Vector, Rect
from djlib.spatial import ExpandingRectTree
from pyshipcommand_gui.viewer import ShipViewer
import pyshipcommand_gui.dialogs as dialogs

from djlib.logger import getLogger
log = getLogger('pyshipcommand.gui.state.viewer')

# TestState
class ViewerState(GameState, INotifyable):

    # CONSTANTS
    SCROLL_RECT_OFFSET = 50
    SCROLL_SPEED = 5
    SHIP_RADIUS = 30

    # ASSETS
    SHIP_IMG = None
    MSHIP_IMG = None

    SUN_COLOR = pygame.Color(255, 255, 0)
    PLANET_COLOR = pygame.Color(0, 128, 255)
    ASTEROID_COLOR = pygame.Color(128, 128, 128)

    SEL_COLOR = pygame.Color(0, 255, 0)

    def __init__(self):
        GameState.__init__(self)

        # Itemize ViewerState members for reference
        self.scroll_rect = None
        self.scroll_dir = None

        self.ship_view = None
        self.sel_ship = None

        self.delay_text = None

        self.universe = None
        self.view_rect = None
        self.tree = None
        self.view_node = None

        self.script_wnd = None
        self.ship_wnd = None
        self.cmd_wnd = None

    def initialize(self):
        """Called the first time the game is changed to this state
           during the applications lifecycle."""
        (w,h) = self.gc.rsurf.get_size()
        self.view_rect = Rect.fromSides(0, 0, w, h)
        self.tree = ExpandingRectTree(Rect.fromSides(0, 0, w, h))

        self.delay_text = []

        # Resources       
        shipImg = resource_stream('pyshipcommand_gui', 'assets/raptor.png')
        shipImg = pygame.image.load(shipImg)
        shipImg = shipImg.convert_alpha()
        self.SHIP_IMG = pygame.transform.rotozoom(shipImg, -90, 0.25)

        cmdImg = resource_stream('pyshipcommand_gui', 'assets/command.png')
        cmdImg = pygame.image.load(cmdImg)
        self.MSHIP_IMG = cmdImg.convert_alpha()

        # Create ship, script, and cmd dialogs
        self._createDialogs()

    def enter(self, client_init):
        """Called every time the game is switched to this state."""
        self.ship_view = ShipViewer(self.gc.rsurf)
        self.ship_view.setClientInit(client_init)
        self.ship_view.SHIP_IMG = self.SHIP_IMG
        self.ship_view.MSHIP_IMG = self.MSHIP_IMG
        self.ship_view.FONT = self.gc.dfont

        self.updateUniverse(client_init.universe)
        self.centerView(client_init.start_pos)

        self.scroll_dir = None

        # Show dialogs
        ui = self.gc.uirender
        ui.add_widget(self.cmd_wnd)
        ui.add_widget(self.script_wnd)
        ui.add_widget(self.ship_wnd)

        # Update dialogs
        self.script_wnd.update_list()

        # Register for input messages
        event_mgr = self.gc.uirender.get_managers()[0]
        event_mgr.add_object(self, SIG_KEYDOWN, SIG_MOUSEDOWN, SIG_MOUSEMOVE)

    def notify(self, event):
        if event.signal == SIG_KEYDOWN:
            # Handle arrow keys
            if event.data.key in (K_UP, K_DOWN, K_RIGHT, K_LEFT):
                self.move(event.data.key)
            elif event.data.key in (K_RETURN, K_SLASH):
                self.gc.uirender.set_active_layer(self.cmd_wnd.depth)
                self.cmd_wnd.set_focus()
                if event.data.key is K_SLASH:
                    self.cmd_wnd.set_text('/')
                    self.cmd_wnd.set_caret(1)
            
        elif event.signal == SIG_MOUSEDOWN:
            # set keyboard focus
            self.gc.uirender.set_active_layer(0)

            if event.data.button == 1: #Left Click
                real_pos = Vector(*event.data.pos) + self.view_rect.pos
                self.sel_ship = self.ship_view.pickShip(real_pos)
                self.ship_wnd.setShip(self.sel_ship)
                self.script_wnd.setShip(self.sel_ship)
                log.debug("Selected %s", str(self.sel_ship))

        elif event.signal == SIG_MOUSEMOVE:
            # Set scroll direction
            mp = event.data.pos
            self.scrollDir = None
            '''
            if not self.scrollRect.collidepoint(mp):
                if mp[0] < self.scrollRect.left:
                    self.scrollDir = K_LEFT
                elif mp[0] > self.scrollRect.right:
                    self.scrollDir = K_RIGHT
                elif mp[1] < self.scrollRect.top:
                    self.scrollDir = K_UP
                elif mp[1] > self.scrollRect.bottom:
                    self.scrollDir = K_DOWN
            '''

    def processInput(self):
        """Called during normal update/render period for this state
           to process it's input."""

        # Handle map scrolling
        if self.scroll_dir:
            self.move(self.scroll_dir)

    def update(self):
        """Called during normal update/render period for this state
           to update it's local or game data."""

        # update the ship view
        self.ship_view.update(self.gc.time_step)

        # Remove any expired delay text
        while len(self.delay_text) > 0 and self.gc.time > self.delay_text[0][0]:
            del self.delay_text[0]

    def render(self):
        """Called during normal update/render period for this state
           to render it's data in a specific way."""
        # get render surface
        rs = self.gc.rsurf
        rs.fill(self.gc.COLOR_BLACK)

        pos = -self.view_rect.pos

        # Draw stars
        self.gc.stars.draw(rs, self.view_rect)

        # Draw universe
        if self.universe:
            uni_node = self.universe.minNode(self.view_rect)
            if not uni_node:
                uni_node = self.universe
            self.ship_view.drawTree(rs, uni_node, pos)

            colors = [self.SUN_COLOR, self.PLANET_COLOR, self.ASTEROID_COLOR]

            for body in uni_node:
                #ignore unknown types
                if body.type < 0:
                    continue

                pygame.draw.circle(rs, colors[body.type], (pos+body.pos).intArgs(), body.radius)
                #pygame.draw.circle(rs, colors[body.type], (100 * body.type,100), body.radius)

        # Draw ships
        self.ship_view.draw(rs, self.view_rect)

        # draw selection
        if self.sel_ship:
            pygame.draw.circle(rs, self.SEL_COLOR, (pos+self.sel_ship.pos).intArgs(), self.SHIP_RADIUS, 2)

        # draw delayed text
        drawPos = Vector(20, self.view_rect.height() - 50)
        for dText in self.delay_text[::-1]:
            self.ship_view.renderText(rs, drawPos.intArgs(), dText[1])
            drawPos -= Vector(0, 20)

        # draw current FPS over everything
        self.ship_view.renderText(rs, (0,0), str(int(self.gc.fpsClock.get_fps())))

        # Now apply UI
        rs.blit(self.gc.uirender.screen, (0,0), None, pygame.BLEND_ADD)

        pygame.display.update()

    def delayedText(self, text, delay):
        if not isinstance(text, list):
            text = [str(text)]
            
        for t in text:
            self.delay_text.append( (self.gc.time+delay, t) )
        log.debug(self.delay_text)

    def leave(self):
        """Called whenever we switch from this state to another."""
        # Unregister for input messages
        event_mgr = self.gc.uirender.get_managers()[0]
        event_mgr.remove_object(self, SIG_KEYDOWN, SIG_MOUSEDOWN, SIG_MOUSEMOVE)

        # Hide dialogs
        ui = self.gc.uirender
        ui.remove_widget(self.cmd_wnd)
        ui.remove_widget(self.script_wnd)
        ui.remove_widget(self.ship_wnd)

        self.ship_view = None

    def shutdown(self):
        """Called during application shutdown."""
        self.font = None
        self.SHIP_IMG = None
        self.MSHIP_IMG = None

    def updateUniverse(self, net_bodies):

        # for now, we only should get one universe update
        # so overwrite any existing data
        w, h = self.view_rect.width(), self.view_rect.height()
        self.universe = ExpandingRectTree(Rect.fromSides(0, 0, w, h))
        body_count = [0]*4
        for body in net_bodies:
            self.universe.insert(body)
            body_count[body.type] += 1
            #print "Body[%d] %s(%d)" % (body.type, str(body.pos), body.radius)

        log.info("Universe contains %d Suns, %d Planets, %d Asteroids, %d Unknowns", *tuple(body_count))

    def updateShips(self, net_ships):
        self.ship_view.updateShips(net_ships)

    def move(self, event):
        vec = Vector(0, 0)
        if event == pygame.K_UP:
            vec = Vector(0, -self.SCROLL_SPEED)
        elif event == pygame.K_DOWN:
            vec = Vector(0, self.SCROLL_SPEED)
            
        if event == pygame.K_LEFT:
            vec = Vector(-self.SCROLL_SPEED, 0)
        elif event == pygame.K_RIGHT:
            vec = Vector(self.SCROLL_SPEED, 0)
            
        self.view_rect.move(vec)
        self.gc.stars.setView(self.view_rect)


    def centerView(self, pos=None):

        # Use mothership pos if no pos is specified
        if not pos:
            pos = self.ships[self.ship_view.mship].pos

        d = pos - self.view_rect.center()
        log.debug("%s -> %s", self.view_rect.center(), pos)
        self.view_rect.move(d)
        log.debug(self.view_rect)
        self.gc.stars.setView(self.view_rect, True)

    def _createDialogs(self):
        (w,h) = self.gc.rsurf.get_size()
        
        # CLI Entry
        edit = widgets.Entry()
        edit.topleft = (0, h-20)
        edit.minsize = w, 20
        edit.depth = 1
        edit.connect_signal(SIG_INPUT, self.processCmd, edit)
        self.cmd_wnd = edit

        # Script widget
        self.script_wnd = dialogs.ScriptWidget(w-205, 0, self.gc.client.scripts,
                                              self.cmd_import,
                                              self.cmd_upload, self.cmd_ship)

        # Ship Info
        self.ship_wnd = dialogs.ShipInfo(w-205, self.script_wnd.height)

    # CMD HANDLERS

    def processCmd(self, edit):

        # Handle edit box
        line = edit.text
        edit.set_text("")
        edit.set_dirty(1)
        
        # Parse the command
        if not line or not len(line) > 0:
            return

        # Raw text should be treated as a chat message.
        if line[0] != '/':
            self.cmd_chat(line)
            return

        # Split line and remove leading '/'
        commandParts = line[1:].split()
        command = commandParts[0].lower()
        args = commandParts[1:]

        # Dispatch the command to the appropriate method.
        try:
            method = getattr(self, 'cmd_' + command)
        except AttributeError, e:
            print 'Error: no such command.'
        else:
            try:
                method(*args)
            except Exception, e:
                    log.error('Error %s', str(e))

    def cmd_help(self, command=None):
        """help [command]: List commands, or show help on the given command"""
        if command:
            print (getattr(self, 'cmd_' + command).__doc__)
        else:
            commands = [cmd[3:] for cmd in dir(self) if cmd.startswith('cmd_')]
            print ("Valid commands: " +" ".join(commands))

    def cmd_exit(self):
        self.gc.quit()

    def cmd_info(self):
        self.gc.client.info()

    def cmd_upload(self, scriptfile):

        # Sanity checks
        if not scriptfile:
            raise Exception("script <script_data>")

        s = self.gc.client.scripts.getScript(scriptfile)
        if s:
            log.info("sending script: %s", s.name)
            self.gc.client.giveScript(s.name, s.source)
            return

        log.error("%s not found!", scriptfile)


    def cmd_import(self, scriptPath = None):

        if not scriptPath:
            script_dlg = dialogs.ImportScriptDialog(self.cmd_import, *self.gc.rsurf.get_size())
            self.gc.uirender.add_widget(script_dlg)
            script_dlg.set_focus()
            return

        # Always treat scripts as a list, even if they only specified a file.
        scriptFiles = [scriptPath]
        if path.isdir(scriptPath):
            scriptFiles = glob(path.join(scriptPath,"*.py"))

        for s in scriptFiles:
            self.gc.client.scripts.importScript(s)

        # Update list contents, even if we could have failed
        if len(scriptFiles) > 0:
            self.script_wnd.update_list()
            #self.script_wnd.select_script(s.name)
            return

        log.error("Could not import: %s", scriptFiles)

    def cmd_reload(self):

        self.gc.client.scripts.reloadScripts()
        self.script_wnd.update_list()


    def cmd_ship(self, cmd, *args):
        log.debug("cmd_ship %s : %s", cmd, str(args))
        if cmd == "create":
            pos = Vector(0.0, 0.0)
            if len(args) == 2:
                pos = Vector(float(args[0]), float(args[1]))
            log.debug(pos)
            self.gc.client.addShip(pos)
        elif cmd == "move":
            ship_id = int(args[0])
            target_pos = Vector(float(args[1]), float(args[2]))
            log.debug(target_pos)
            self.gc.client.moveShip(ship_id, target_pos)
        elif cmd == "orbit":
            ship_id = int(args[0])
            target_pos = Vector(float(args[1]), float(args[2]))
            radius = float(args[3])
            self.gc.client.orbitShip(ship_id, target_pos, radius)
        elif cmd == "follow":
            ship_id = int(args[0])
            target_id = int(args[1])
            dist = float(args[2])
            self.gc.client.followShip(ship_id, target_id, dist)
        elif cmd == "script":
            ship_id = int(args[0])
            script_name = args[1]

            action = "upgrade" # default to an upgrade
            if len(args) > 2:
                action = args[2]

            self.gc.client.scriptShip(ship_id, script_name, action)

    def cmd_scroll(self, *args):
        scroll_speed = ShipViewer.SCROLL_SPEED
        if len(args) == 1:
            scroll_speed = int(args[0])
        ship_view.SCROLL_SPEED = scroll_speed

    def cmd_msg(self, *args):
        log.debug("cmd_msg: %s", str(args))
        self.gc.client.giveMessage(args)

    def cmd_home(self):
        ship_view.centerView()

    def cmd_chat(self, msg):
        self.gc.client.giveChat(msg)

    def cmd_players(self):
        self.gc.client.getPlayers()
        
#end ViewerState

