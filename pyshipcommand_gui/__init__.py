
# Install threadedselectreactor to integrate with ocempGUI
from twisted.internet._threadedselect import install as reactor_install
reactor_install()

# SYSTEM
from os import path
from glob import glob
import sys

# ADDITIONAL LIBRARIES
from twisted.internet import reactor, task
import pygame
from ocempgui.widgets import TwistedRenderer

# PYSHIPCOMMAND
from djlib.game import GameClass
import djlib.logger as logger
log = logger.getLogger('pyshipcommand.gui')

from cscripts import ClientScriptMgr
from client import GUIClient
from viewer import StarGenerator
from states.login import LoginState

# CONSTANTS
from pyshipcommand.server import DEFAULT_PORT
DESIRED_FPS = 30.0
SCREEN_SIZE = (800, 600)
DEFAULT_SERVER_STEP = 1.0

class GUIGameClass(GameClass):

    def __init__(self):
        GameClass.__init__(self)

        # Global constants
        self.DEFAULT_SERVER = "localhost"
        self.DEFAULT_PORT = DEFAULT_PORT
        self.DEFAULT_USER = "admin"
        self.COLOR_BLACK = pygame.Color(0,0,0)
        self.STAR_COLOR = pygame.Color(255, 255, 255)
        self.SERVER_STEP = DEFAULT_SERVER_STEP

        # Itemize GameClass variables for reference
        self.rsurf = None
        self.uisurf = None
        self.uirender = None
        self.fpsClock = None
        self.client = None
        self.stars = None
        self.time = 0
        self.time_step = 0
        self.dfont = None

    def initialize(self):

        # Init pygame and create main rendering surface
        pygame.init()
        self.rsurf = pygame.display.set_mode(SCREEN_SIZE)
        pygame.display.set_caption("pyShipCommand")

        # Create UI Renderer that renders to a clone of the main surface
        self.uirender = TwistedRenderer()
        self.uisurf = self.rsurf.copy()
        self.uirender.set_screen(self.uisurf)
        self.uirender.set_color((0, 0, 0))
        self.uirender.reactor = reactor

        # Main Game Clock
        self.fpsClock = pygame.time.Clock()

        # Network client
        self.client = GUIClient(self)

        # The Starfield is used by multiple states and should be created now
        self.stars = StarGenerator((SCREEN_SIZE[0]/2, SCREEN_SIZE[1]/2),
                                        SCREEN_SIZE, 25)

        # Load the default font
        self.dfont = pygame.font.Font(pygame.font.get_default_font(), 12)
        if not self.dfont:
            log.error("Default Font not loaded!")

        # Be sure to initialize base GameClass
        GameClass.initialize(self)

    def update(self):
        # Add any global/game specific updates here
        self.fpsClock.tick()
        self.time = pygame.time.get_ticks()
        self.time_step = self.fpsClock.get_rawtime()/1000.0

        GameClass.update(self)

    def shutdown(self):
        GameClass.shutdown(self)

        self.client.disconnect()
        pygame.quit()

    def quit(self):
        # tell systems to shut down
        # normally we would just set self.running to False
        # but our update loop is based on the Twisted Reactor
        # Use pygame to give it the QUIT signal
        e = pygame.event.Event(pygame.QUIT)
        pygame.event.post(e)

#end GUIGameClass

def main(argv=sys.argv[1:]):
    from argparse import ArgumentParser, FileType

    # Argument parsing
    args = ArgumentParser()
    logger.add_log_args(args)

    options = args.parse_args(argv)
    logger.set_log_options(options, 'pyshipcommand')

    # MAIN GAMECLASS
    game = GUIGameClass()
    
    #shipImg = resource_stream('pyshipcommand_gui', 'assets/raptor.png')
    #shipImg = pygame.image.load(shipImg)
    #shipImg = shipImg.convert_alpha()
    #ship_view.SHIP_IMG = pygame.transform.rotozoom(shipImg, -90, 0.25)

    #cmdImg = resource_stream('pyshipcommand_gui', 'assets/command.png')
    #cmdImg = pygame.image.load(cmdImg)
    #ship_view.MSHIP_IMG = cmdImg.convert_alpha()

    # Manually connect
    #client.connect(SERVER_ADDR, SERVER_PORT)
    #client.login(DEFAULT_USER, DEFAULT_PASS)

    # Initialize
    game.initialize()

    # Create and switch to starting state
    game.changeState(LoginState)
    
    # Update loop is slightly different than normal.
    # Instead of a running while True loop, we register
    # an asynchronous task that runs at our desired frame rate
    update_task = task.LoopingCall(game.update)
    update_task.start(1.0/DESIRED_FPS)

    # blocks until reactor is shutdown.
    game.uirender.start()

    update_task.stop()
    game.shutdown()
#end main

if __name__ == "__main__":
    main()
#end __main__
