
# ADDITIONAL LIBRARIES
from ocempgui import widgets
from ocempgui.widgets.Constants import *

# LOCAL
from djlib.logger import getLogger
log = getLogger('pyshipcommand.gui.dialogs')

class ShipInfoWidget(widgets.Table):
    
    FRAME_WIDTH = 200
    FRAME_HEIGHT = 125
    
    def __init__(self, ship = None):
        widgets.Table.__init__(self, 5, 2)

        self.add_child(0, 0, widgets.Label("Ship Id:"))
        self.add_child(1, 0, widgets.Label("Position:"))
        self.add_child(2, 0, widgets.Label("Speed:"))
        self.add_child(3, 0, widgets.Label("Rot:"))
        self.add_child(4, 0, widgets.Label("Target:"))
        
        for row in xrange(self.rows):
            entry = widgets.Entry()
            entry.editable = False
            entry.caret_visible = False
            self.add_child(row, 1, entry)
            
        self.set_column_align(0, ALIGN_RIGHT)
            
        self.ship = ship
        self.populate()
        
    def populate(self):
        if self.ship:
            pos = "<%d, %d>" % self.ship.pos.intArgs()
            self._setData(self.ship.id, pos, self.ship.speed,
                          int(self.ship.rotation), self.ship.recent_attack)
        else:
            self._setData("", "", "", "", "")

    def setShip(self, ship):
        if self.ship != ship:
            self.ship = ship
            self.populate()
            
    def _setData(self, *datas):
        for i in xrange(len(datas)):
            self.grid[(i, 1)].set_text(str(datas[i]))
    
#end ShipInfo


class ShipInfo(widgets.Window):
    
    def __init__(self, x, y, ship = None):
        widgets.Window.__init__(self, "Ship View")
        self.topleft = (x, y)
        self.child = ShipInfoWidget(ship)

        # All Widgets get depth of 1 to set it above the viewer
        self.depth = 1
        
    def populate(self):
        self.child.populate()

    def setShip(self, ship):
        self.child.setShip(ship)

#end ShipInfo


class LoginDialog(widgets.DialogWindow):
    def __init__(self, client, registrar, screen_width, screen_height):
        widgets.DialogWindow.__init__(self, "Login")
        self.child = widgets.Table(7,1)

        # All Widgets get depth of 1 to set it above the viewer
        self.depth = 1

        self.client = client
        self.registrar = registrar
        
        # Server name/port
        hframe = widgets.HFrame(widgets.Label("Server:"))
        self.serverEntry = widgets.Entry()
        hframe.add_child(self.serverEntry)
        hframe.add_child(widgets.Label(":"))
        self.portEntry = widgets.Entry()
        self.portEntry.minsize = (40, self.portEntry.height)
        hframe.add_child(self.portEntry)
        self.child.add_child(0, 0, hframe)
        
        # Username Entry
        hframe = widgets.HFrame(widgets.Label("Username:"))
        self.userEntry = widgets.Entry()
        hframe.add_child(self.userEntry)
        self.child.add_child(2, 0, hframe)
        
        # Password Entry
        hframe = widgets.HFrame(widgets.Label("Password:"))
        self.passEntry = widgets.Entry()
        self.passEntry.password = True
        self.passEntry.connect_signal(SIG_INPUT, self._loginCallback)
        hframe.add_child(self.passEntry)
        self.child.add_child(3, 0, hframe)
        
        # Login Button
        self.loginBtn = widgets.Button("Login")
        self.loginBtn.minsize = (hframe.width, self.loginBtn.minsize[1])
        self.loginBtn.connect_signal(SIG_CLICKED, self._loginCallback)
        #self.loginBtn.sensitive = False
        self.child.add_child(4, 0, self.loginBtn)

        # Register Button
        self.regBtn = widgets.Button("Register")
        self.regBtn.minsize = (hframe.width, self.loginBtn.minsize[1])
        self.regBtn.connect_signal(SIG_CLICKED, self._registerCallback)
        self.child.add_child(5, 0, self.regBtn)
        
        # Exit
        exitBtn = widgets.Button("Quit")
        exitBtn.minsize = (hframe.width, exitBtn.minsize[1])
        exitBtn.connect_signal(SIG_CLICKED, self._quit)
        self.child.add_child(6, 0, exitBtn)
        
        # Position dialog in middle/lower half of screen
        self.topleft = ((screen_width-self.width)/2, screen_height/2)
        
    def populateUser(self, username):
        self.userEntry.set_text(username)
        
    def populateServer(self, name, port):
        self.serverEntry.set_text(name)
        self.portEntry.set_text(str(port))

    def getUserName(self):
        return self.userEntry.text
        
    def _loginCallback(self):
        
        # Check for valid entries
        server = self.serverEntry.text
        port = int(self.portEntry.text)
        if server == "" or port == 0:
            return
        
        username = self.userEntry.text
        password = self.passEntry.text
        if username == "" or password == "":
            return
        
        self.client.connect(server, port)
        self.client.login(username, password)

    def _registerCallback(self):

        # Check for valid entries
        server = self.serverEntry.text
        port = int(self.portEntry.text)
        if server == "" or port == 0:
            return
        
        username = self.userEntry.text
        password = self.passEntry.text
        if username == "" or password == "":
            return

        self.registrar.connect(server, port)
        self.registrar.login(username, password)

        
    def _quit(self):
        self.client.exit()
        
#end LoginDialog


from pygame import K_UP, K_DOWN, K_LEFT, K_RIGHT
class MovementWidget(widgets.Table):

    def __init__(self, x, y, callback):
        widgets.Table.__init__(self, 3, 3)

        # All Widgets get depth of 1 to set it above the viewer
        self.depth = 1
        self._signals[SIG_KEYDOWN] = None # Dummy for keyboard activation.
        self.cb = callback
        
        # Create child buttons
        buttons = []
        buttons.append(widgets.Button("Up"))
        buttons.append(widgets.Button("Down"))
        buttons.append(widgets.Button("Left"))
        buttons.append(widgets.Button("Right"))
        
        for btn in buttons:
            btn.minsize = (20, 20)
            btn.connect_signal(SIG_CLICKED, self._buttonCB, btn)
            
        self.add_child(0, 1, buttons[0])
        self.add_child(2, 1, buttons[1])
        self.add_child(1, 0, buttons[2])
        self.add_child(1, 2, buttons[3])
        
        
    def notify(self, event):
        
        if event.signal == SIG_KEYDOWN:
            # Handle arrow keys
            if event.data.key in (K_UP, K_DOWN, K_RIGHT, K_LEFT):
                self.cb(event.data.key)
                
        widgets.Table.notify(self, event)
        
    def _buttonCB(self, button):
        log.debug(button.text)
#end MovementWidget


class ImportScriptDialog(widgets.FileDialog):

    def __init__(self, callback, screen_width, screen_height):
        self._callback = callback

        buttons = [widgets.Button("#OK"), widgets.Button("#Cancel")]
        buttons[0].minsize = 80, buttons[0].minsize[1]
        buttons[1].minsize = 80, buttons[1].minsize[1]
        results = [DLGRESULT_OK, DLGRESULT_CANCEL]

        widgets.FileDialog.__init__(self, "Select your file(s)", buttons, results)
        self.depth = 1 # Make it the top window.
        self.filelist.selectionmode = SELECTION_MULTIPLE
        self.connect_signal(SIG_DIALOGRESPONSE, self._fileSelectCB)

        # Center dialog on screen
        self.topleft = ((screen_width-self.width)/2, (screen_height-self.height)/2)

    def _fileSelectCB(self, result):
        if result == DLGRESULT_OK:
            for file in self.get_filenames():
                self._callback(file)
        self.destroy()
#end ImportScriptDialog

from ocempgui.widgets.components import TextListItem
class ScriptWidget(widgets.Table):

    def __init__(self, x, y, s_mgr, import_cb, upload_cb, bind_cb):
        widgets.Table.__init__(self, 2, 2)
        self.topleft = (x, y)
        self.smgr = s_mgr
        self.import_cb = import_cb
        self.upload_cb = upload_cb
        self.bind_cb = bind_cb
        self.bind_ship = None

        # All Widgets get depth of 1 to set it above the viewer
        self.depth = 1
        self.spacing = 5

        # Create a simple ScrolledList
        vframe = widgets.VFrame()
        vframe.border = BORDER_NONE

        self.scrollWnd = widgets.ScrolledList(200, 200)

        frame = widgets.HFrame()
        frame.border = BORDER_NONE
        frame.spacing = 5
        button1 = widgets.Button("#Import")
        button1.connect_signal(SIG_CLICKED, self._import, self.scrollWnd)
        button2 = widgets.Button("#Upload")
        button2.connect_signal(SIG_CLICKED, self._upload_item, self.scrollWnd, button2)
        button2.sensitive = False
        self.bind_button = widgets.Button("#Bind")
        self.bind_button.connect_signal(SIG_CLICKED, self._bind_item, self.scrollWnd, self.bind_button)
        self.bind_button.sensitive = False
        self.scrollWnd.connect_signal(SIG_SELECTCHANGED, self._update_button, self.scrollWnd, button2)
        self.scrollWnd.connect_signal(SIG_SELECTCHANGED, self._update_button, self.scrollWnd, self.bind_button)
        frame.add_child(button1, button2, self.bind_button)
        vframe.add_child(self.scrollWnd, frame)

        self.add_child (0, 0, vframe)

        # Create a ScrolledList with always scrolling and single selection.
        # always = ScrolledList (200, 200)
        # always.scrolling = SCROLL_ALWAYS
        # always.selectionmode = SELECTION_SINGLE
        # for i in xrange (15):
        #     item = None
        #     if i % 2 == 0:
        #         item = TextListItem ("Short item %d" % i)
        #     else:
        #         text = "Very, " +3 * "very, very," + "long item %d" % i
        #         item = TextListItem (text)
        #     always.items.append (item)
        # table.add_child (0, 1, always)
        # table.set_align (0, 1, ALIGN_TOP)

    def update_list(self):
        # Remove all existing items
        del self.scrollWnd.items[:]

        scripts = self.smgr.getScriptList()
        for s in scripts:
            script = self.smgr.getScript(s)
            self.scrollWnd.items.append(TextListItem("%s (%d)" % (s, script.version)))
        return len(scripts)

    def select_script(self, script):
        for i in self.scrollWnd.items:
            if i.text == script:
                log.debug("Selecting %s", script)
                self.scrollWnd.select(i)
                return

    def setShip(self, ship):
        self.bind_ship = ship
        self._update_button(self.scrollWnd, self.bind_button)

    def _import(self, slist):
        # Callback should open Import Dialog on blankcallback.
        # It should also call update_list() as it is used by the CLI as well.
        self.import_cb()

    def _upload_item(self, slist, button):
        for s in slist.get_selected():
            self.upload_cb(s.text.split()[0])
        button.sensitive = False

    def _update_button(self, slist, button):
        if button is self.bind_button:
            button.sensitive = len(slist.get_selected()) and self.bind_ship
        else:
            button.sensitive = len(slist.get_selected()) > 0

    def _bind_item(self, slist, button):
        sel = slist.get_selected()
        if self.bind_ship and len(sel) == 1:
            self.bind_cb("script", str(self.bind_ship.id), sel[0].text.split()[0])
        button.sensitive = False

#end ScriptWidget

