
import pygame, sys
from pygame.locals import *

# Init pygame and create main rendering surface
pygame.init()
g_rsurf = pygame.display.set_mode((800, 600))
pygame.display.set_caption("ui_test")

fpsClock = pygame.time.Clock()

# Rendering constants
CLEAR_COLOR = pygame.Color(0, 0, 0)
SPATIAL_COLOR = pygame.Color(50, 50, 50)
FONT_COLOR = pygame.Color(255, 255, 255)

from djlib.spatial import RectTree
from djlib.primitives import Rect, Vector, Entity
tree = RectTree(Rect.fromSides(0, 0, 16000, 12000))
screen_rect = Rect.fromSides(100, 100, 700, 500)
draw_all = False
full_cull = False
draw_count = 0
offset = Vector(0, 0)

shipImg = pygame.image.load("assets/raptor.png")
shipImg = pygame.transform.rotozoom(shipImg, 0, 0.1)
#shipImg = shipImg.convert_alpha()


font = pygame.font.Font(pygame.font.get_default_font(), 12)
def renderText(rs, pos, text, centered = False):
    surf = font.render(text, False, FONT_COLOR)
    if centered:
        pos = (pos[0]-surf.get_width()/2, pos[1]-surf.get_height()/2)
    rs.blit(surf, pos)

def drawTree(rs, node):
    rect = pygame.Rect((offset+node.rect.pos).intArgs(), node.rect.size.intArgs())
    pygame.draw.rect(rs, SPATIAL_COLOR , rect, 2)
    if node.data and not full_cull:
    	renderText(rs, (offset+node.rect.pos).intArgs(), str(len(node.data)))
    	for pt in node.data:
    		rs.blit(shipImg, (offset+pt.pos).intArgs())
    if node.children:
        for child in node.children:
            drawTree(rs, child)


def draw(rs):
	rs.fill(CLEAR_COLOR)

	rect = pygame.Rect(screen_rect.pos.intArgs(), screen_rect.size.intArgs())
	pygame.draw.rect(rs, FONT_COLOR, rect, 3)

	rect = Rect.fromPointSize(screen_rect.pos-offset, screen_rect.size[0], screen_rect.size[1])
	node = tree if draw_all else tree.minNode(rect)
	if not node:
		node = tree
	drawTree(rs, node)

	if full_cull:
		#data = node.getData(rect)
		for pt in node:
			rs.blit(shipImg, (offset+pt.pos).intArgs())

	#render FPS over everything
	renderText(rs, (0, 0), "FPS: %d - %d" % (fpsClock.get_fps(), draw_count))
	renderText(rs, (0, 15), "Draw All(r): %s - Full Cull(c): %s" % (str(draw_all), str(full_cull)))
	renderText(rs, (0, 30), str(offset))

def handleEvent(event):
	global offset

	if event.type == KEYDOWN:
		if event.key == K_r:
			global draw_all
			draw_all = not draw_all
		elif event.key == K_c:
			global full_cull
			full_cull = not full_cull
		elif event.key == K_RIGHT:
			offset += Vector(10, 0)
		elif event.key == K_LEFT:
			offset += Vector(-10, 0)
		elif event.key == K_UP:
			offset += Vector(0, -10)
		elif event.key == K_DOWN:
			offset += Vector(0, 10)

import random
# main render loop
while True:
	#pygame updates
	for event in pygame.event.get():
		if event.type == QUIT:
			pygame.quit()
			sys.exit()

		handleEvent(event)

	sides = tree.rect.sides()
	tree.insert(Entity(Vector(random.randint(sides[0], sides[2]), random.randint(sides[1], sides[3]))))
	draw_count+=1

	draw(g_rsurf)
	pygame.display.update()
	fpsClock.tick()
