
# SYSTEM
import os.path

# ADDITIONAL LIBRARIES

# PYSHIPCOMMAND
from djlib.logger import getLogger
log = getLogger('pyshipcommand.gui.cscripts')

from pyshipcommand.scripts import Script, ScriptMgr

# CONSTANTS
SCRIPT_DIR = "cscripts"

class ClientScriptMgr:

	def __init__(self):
		# Inheritance vs containment?
		self.smgr = ScriptMgr()
		self.smgr.script_dir = os.path.join(os.getcwd(), SCRIPT_DIR)

		self.curr_player = None
		self.s_scripts = None

	def setServerScripts(self, net_scripts):
		# construct script dict from scripts list
		self.s_scripts = {}
		for s in net_scripts:
			self.s_scripts[s.name] = s

			ls = self.getScript(s.name)
			if ls:
				if ls.lastHash == s.last_hash:
					ls.version = s.version
				else:
					log.error("Script %s[%s] not in sync with server [%s]!", ls.name, ls.lastHash, s.last_hash)

	def setActivePlayer(self, player):

		if player == self.curr_player:
			return

		# Purge any existing player
		self.smgr.clear()

		# Attempt to load player scripts from local script dir
		self.curr_player = player
		self.reloadScripts()

	def getScriptList(self):
		if not self.curr_player:
			return None
		return self.smgr._getPlayerScripts(self.curr_player).keys()

	def giveScript(self, name, data):
		self.smgr.giveScript(self.curr_player, name, data)

	def getScript(self, name):
		return self.smgr.getScript(self.curr_player, name)

	def importScript(self, scriptfile):
		if not self.curr_player or not scriptfile:
			return None

		with open(scriptfile) as f:
			name = os.path.splitext(os.path.basename(scriptfile))[0]
			data = f.read()

			log.debug("importing script: %s", name)
			self.giveScript(name, data)
			return self.getScript(name)
		return None

	def reloadScripts(self):
		if not self.curr_player:
			return 0

		player_dir = os.path.join(self.smgr.script_dir, self.curr_player)
		return self.smgr._loadPlayerScripts(self.curr_player, player_dir)
#end ClientScriptMgr


	


