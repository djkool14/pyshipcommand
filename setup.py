#!/usr/bin/env python
from setuptools import setup, find_packages

path = 'pyshipcommand'

def get_version():
    import os
    
    d = {}

    # handle single file modules
    if os.path.isdir(path):
        module_path = os.path.join(path, '__init__.py')
    else:
        module_path = path
                                            
    with open(module_path) as f:
        try:
            exec(f.read(), None, d)
        except:
            pass

    return d.get("__version__", 0.1)

## Use py.test for "setup.py test" command ##
from setuptools.command.test import test as TestCommand
class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True
    def run_tests(self):
        #import here, cause outside the eggs aren't loaded
        import pytest
        pytest.main(self.test_args)

## Try and extract a long description ##
for readme_name in ("README", "README.rst", "README.md"):
    try:
        readme = open(readme_name).read()
    except (OSError, IOError):
        continue
    else:
        break
else:
    readme = ""

## Finally call setup ##
setup(
    name = 'pyshipcommand',
    packages = ['pyshipcommand', 'pyshipcommand_gui', 'djlib'],
    version = get_version(),
    author = "djkool14",
    description = "Online Persistent game",
    long_description = readme,
    license = "GPLv3",
    keywords = "game online persistent world space",
    url = "http://bitbucket.org/djkool14/pyshipcommand",

    entry_points = {"console_scripts":["pyship-server=pyshipcommand.server:main",
                                       "pyship-gui=pyshipcommand_gui:main",
                                      ]
                   },

    # include any extra files found in the package dir
    include_package_data = True,
    
    # extra data files needed by the program that is tightly coupled (eg translation tables
    package_data = {'example_scripts':['mship_cmd.py',
                                       'ship_cmd.py',
                                       'tracker.py'],
                    'pyshipcommand_gui':['assets/command.png',
                                         'assets/raptor.png',
                                         'assets/title_logo.png'],
                   },

    # files not in the module/packge dir that are needed, eg art assets,README files, init scripts
    # relative paths get installed relative tot he install dir while absolute paths are installed to
    # the expected location on the os
    # data_files = [('bitmaps', ['bm/*.gif', 'bm/*.jpg'])
    #              ('/etc/init.d', ['init-scripts'])
    #             ],
    
    # if not using entry points then scripts can be sed to generate programs that appear on $PATH
    # if the first line starts with #! and contains 'python' it will be rewritten by distutils to
    # point to the correct python
    # scripts = ['scripts/dosomthing'],
    
    # fine for pure python modules otherwise disable this unless you know what you are doing
    # if you need package_data/resources and you are not using the helpers but open() instead
    # (as well as some fancy autopath detection) then disable this option
    zip_safe = True,
    
    # needed if you are using distutils extensions for the build process
    # setup_requires = ['hgdistver'],

    # optinal packages needed to install/run this app
    install_requires = ['Twisted>=12.0.0',
                        'zope.interface==3.8.0',
                        'zope.event==3.5.2',
                        'zope.component==3.12.1',
                        'RestrictedPython==3.5.2',
                        'OcempGUI==0.2.9',
                        'distribute',
                       ],

    # extra packages needed for the test suite
    tests_require = ['pytest'],
    cmdclass = {'test': PyTest},
    
    # Extra locations to look for dependincies if not found apart from PyPi
    dependency_links = ['https://sourceforge.net/projects/ocemp/files/ocempgui/0.2.9/OcempGUI-0.2.9.tar.gz#egg=OcempGUI-0.2.9',],
)
