
from zope.interface import implements, Interface


def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    enums["debug"] = sequential
    return type('Enum', (), enums)


class IAllocator(Interface):
    def allocate(self, reserve):
        """Allocates a unique object, or reserves space for an existing one."""

    def deallocate(self, value):
        """Deallocates a previously allocated or reserved object for reuse."""
#end IAllocator


class IdAllocator:
    implements(IAllocator)

    def __init__(self, start_id = 0, end_id = None):
        if end_id:
            assert start_id < end_id
        self.start = start_id
        self.end = end_id
        self.curr = start_id

    def allocate(self, reserve = None):
        if self.end and self.curr > self.end:
            return None #Generator is empty
            
        if reserve and self.curr < reserve:
            self.curr = reserve + 1
        else:
            reserve = self.curr
            self.curr = self.curr + 1

        return reserve

    def deallocate(self, value):
        #deallocate does nothing for a basic allocator
        pass
#end IdAllocator


class IManager(Interface):
    """A global system Manager"""

class IPersistent(Interface):

    def load(stream):
        """Load Manager state from any data stream."""

    def save(stream):
        """Save Manager state to any data stream."""
        
#end IPersistent

class IGame(Interface):
    """The Game Singleton"""

