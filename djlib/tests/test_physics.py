from djlib.physics import PhysicsEntity, Attributes
from djlib.primitives import Entity, Vector


def test_physics_entity():
    time = 1 # second
    intervals = 10

    e = PhysicsEntity(Vector(0,0), Attributes())
    e.setVelocity(Vector(10, 10))
    
    [e.update(interval) for interval in range(time/intervals)]

    assert e.vel.x == 10
    assert e.vel.y == 10


