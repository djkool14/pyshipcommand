from djlib.primitives import Vector, Size

def approx(dec_points=3):
    """check 2 floating point numbers are aproximatly equal
    dec_points specifies the number of decimal points to check to
    returns a function that can be used to compare 2 (floating point)
    values
    
    function returned can also take an optinal 'reason' that is passed
    to the assert statment
    """
    # Check that numbers are close to the expected value
    # as diffrent python versions/implementations return
    # diffrent values for floating point numbers
    def compare(a, b, reason=None):
        val = abs(b - a)
        assert val < (10 ** -dec_points), reason
    return compare
approx = approx()
    
def pytest_funcarg__vector(request):
    """Dependency injection of a standardised vector"""
    return Vector(1,2,3)

def pytest_funcarg__size(request):
    """Dependency injection of a standardised size"""
    return Size(1,2,3)

## Vector ##
def test_vector_repr(vector):
    """Check that the repr string creation code works, 
    commonly overlooked"""
    r = repr(vector)

def text_vector_addition(vector):
    v = vector + vector
    assert v == Vector(2,4,6)

def text_vector_subtraction(vector):
    v = vector - vector
    assert v == Vector(0,0,0)

def text_vector_multiplaction(vector):
    v = vector * vector
    assert v == 14

def text_vector_addition(vector):
    v = vector * 5
    assert v == Vector(5,10,15)

def test_vector_attributes(vector):
    # we are only testing the implementation, not that the 
    # returned values are correct here as unlike the other
    # operations there should be no calculations, only
    # storage
    assert vector.x == vector.x
    assert vector.y == vector.y
    assert vector.z == vector.z

def test_vector_length(vector):
    l = vector.length()
    approx(l, 3.742)

def test_vector_normalized(vector):
    vector = vector.normalized()
    v = Vector(0.267, 0.534, 0.801)
    approx(vector.x, v.x)
    approx(vector.y, v.y)
    approx(vector.z, v.z)

def test_vector_clear(vector):
    vector.clear()
    assert vector == Vector(0,0,0)

## Size ##
def test_size_repr(size):
    """Check that the repr string creation code works, 
    commonly overlooked"""
    r = repr(size)

def test_size_attributes(size):
    # we are only testing the implementation, not that the 
    # returned values are correct here as unlike the other
    # operations there should be no calculations, only
    # storage
    assert size.w == size.w, 'No Width attribute found'
    assert size.h == size.h, 'No Height attribute found'
    assert size.l == size.l, 'No Length attribute found'


#def test_bounding_volumes():
#    print "BOUND VOLUMES TEST".center(30, "=")


