from djlib.utils import IdAllocator

def unit_idallocator():
    """Test basic functionality of idAllocator with no args"""
    ids = IdAllocator()
    for i in range(10):
        assert ids.allocate() == i
    base = i

    # ensure the next 100 ids are reserved
    ids.allocate(100)
    base += 100
    
    for i in range(5):
        assert ids.allocate() == base + i


def test_idallocator_range():
    """Test idAllocator with a specified range, testing both the
    start and end value
    """
    start, end = 5, 10

    ids = IdAllocator(start, end)
    for i in range(start, end + 1):
        assert ids.allocate() == i
    
    # We have gone past the end, should return None
    assert ids.allocate() == None
