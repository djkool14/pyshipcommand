from djlib.primitives import Point, Rect, Entity
from djlib.spatial import RectTree, ExpandingRectTree

def test_rect_tree():
    RectTree.MAX_DATA = 2
    rect = Rect.fromSides(-1000, -1000, 1000, 1000)
    tree = RectTree(rect)

    entities = ( Entity(Point(100, 100)),
                 Entity(Point(-100, -100)),
                 Entity(Point(200, 200)),
                 Entity(Point(600, 600)) )

    tree.insert(entities[0])
    tree.insert(entities[1])
    print tree

    tree.insert(entities[2])
    tree.insert(entities[3])
    print tree

    tree.remove(entities[3])
    tree.remove(entities[2])
    print tree


def test_expanding_rect_tree():
    rect = Rect.fromSides(-1000, -1000, 1000, 1000)

    extree = ExpandingRectTree(rect)
    print extree

    extree.insert(Entity(Point(-1500, -1500)))
    print extree

    extree.insert(Entity(Point(1500, 1500)))
    print extree



