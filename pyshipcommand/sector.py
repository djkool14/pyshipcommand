
from djlib.primitives import Rect

from logging import getLogger
log = getLogger('pyshipcommand.sector')


class Sector(Rect):
    def __init__(self, pos, size, contents = []):
        Rect.__init__(self, pos, size)
        self.contents = contents
        
        
class SectorTree(Sector):
    def __init__(self, sector_size, init_area):
        self.sector_size = sector_size
        self.setArea(init_area)
        
    def setArea(self, area_rect):
        if self.contains(area_rect):
            return
            
        # More Sectors need to be created
                
        
    def getContents(self, rect):
        sectors = self.getSectors(rect)
        contents = []
        
        for sector in sectors:
            contents.expand(sector.contents)
            
        return contents
    
    def getSectors(self, rect):
        pass
        
        
        
    def _generateContents(self, sector):
        assert len(sector.contents) == 0
        return []
    
#end SectorTree
        
    
