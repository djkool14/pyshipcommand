
# !!!!! Everything in this file should be player script safe !!!!!!!

from zope.interface import implements
from djlib import primitives
from djlib.utils import IManager

from logging import getLogger
log = getLogger('pyshipcommand.equipment')


class Type(object):
    INVALID = ""

    # Base Equipment
    ENGINE = "engine"
    RADAR = "radar"
    COMM = "comm"

    # Auxilary
    WEAPON = "weapon"
    SHIELD = "shield"
    TOOL = "tool"
    FACTORY = "factory"
#end Type

# Equipment Data
# Share Database class for equipment constants
class EquipmentData(object):
    def __init__(self, type_, **more_data):
        # methods don't need to be _protected as EquipmentData
        # should itself be _protected within the equipment.
        self.type = type_
        #self.id = id_
        #self.name = name
        #self.rank = rank
        #self.size = size
        self.__dict__.update(more_data)
#end EquipmentData

class Equipment(object):

    TYPE = Type.INVALID
    REQ_DATA = {'type', 'id', 'name', 'rank', 'size'}

    @classmethod
    def validateData(cls, equip_data):
        missing = cls.REQ_DATA - equip_data.__dict__.viewkeys()
        if len(missing):
            log.error("Equipment %s missing data %s", cls.TYPE, str(missing))
        if equip_data.type != cls.TYPE:
            log.error("Equipment %s is not of expected type %s", equip_data.type, cls.TYPE)
            return False
        return len(missing) == 0

    def __init__(self, equip_data):
        assert self.validateData(equip_data), "Equipment Data is invalid"
        self._data = equip_data
        self._ship = None

    def getType(self):
        return self._data.type

    def getName(self):
        return self._data.name

    def getRank(self):
        return self._data.rank

    def getSize(self):
        return self._data.size

    def _install(self, ship):
        self._ship = ship

    def _update(self, dt):
        pass
#end Equipment


class BaseEquipment(Equipment):
    pass
#end BaseEquipment


class Engine(BaseEquipment):

    TYPE = Type.ENGINE
    REQ_DATA = Equipment.REQ_DATA | {'thrust'}

    def __init__(self, equip_data):
        Equipment.__init__(self, Type.ENGINE, equip_data)

    def getThrust(self):
        return self._data.thrust
#end Engine


class Radar(BaseEquipment):

    TYPE = Type.RADAR
    REQ_DATA = Equipment.REQ_DATA | {'range'}

    class Entity(primitives.Entity):
        def __init__(self, type_, id_, pos_, radius_):
            primitives.Entity.__init__(self, pos_)
            self.type = type_
            self.id = id_
            self.radius = radius_
    #end Radar.Entity

    def __init__(self, equip_data):
        Equipment.__init__(self, equip_data)

    def getRange(self):
        return self._data.range

    def detectEntities(self, type_ = "all", dist = 0):
        #Set or cap radar distance
        if dist == 0 or dist > self.getRange():
            dist = self.getRange()

        # Convert default
        if type_ == "all":
            type_ = None

        entities = self._ship.getEntityInRange(dist, type_)
        return [Radar.Entity(entity.__class__.__name__, entity.id, entity.getPosition(), entity.radius) for entity in entities]
#end Radar


class Communication(BaseEquipment):

    TYPE = Type.COMM
    REQ_DATA = Equipment.REQ_DATA | {'range', 'limit'}

    def __init__(self, equip_data):
        Equipment.__init__(self, equip_data)

    def getRange(self):
        return self._data.range

    def send(self, target_id, msg):
        if len(msg) > self._data.limit:
            return False
        return self._ship.sendMessage(target_id, self.getRange(), msg)

    def queryMessages(self):
        return self._ship._getMessages()
#end Communication


class Weapon(BaseEquipment):

    TYPE = Type.WEAPON
    REQ_DATA = Equipment.REQ_DATA | {'dmg', 'rate', 'range'}

    def __init__(self, equip_data):
        Equipment.__init__(self, equip_data)
        self._target = None
        self._reload = 0

    def getDamage(self):
        return self._data.dmg

    def getRate(self):
        return self._data.rate

    def getRange(self):
        return self._data.range

    def setTarget(self, target_id):
        if hasattr(target_id, "id"):
            target_id = target_id.id
        self._target = target_id

    def getTarget(self):
        return self._target

    def canShoot(self):
        return self._reload <= 0.0

    def shoot(self, target_id = None):
        if not self.canShoot():
            return False

        if not target_id:
            target_id = self._target

        if target_id:
            self._reload = self.getRate()
            return self._ship.shoot(target_id, self.getDamage(), self.getRange())
        return False

    def _update(self, dt):
        self._reload = (self._reload - dt if self._reload > 0.0 else 0.0)
#end Weapon


class AuxEquipment(Equipment):
    pass
#end AuxEquipment


class Shield(AuxEquipment):

    TYPE = Type.SHIELD
    REQ_DATA = Equipment.REQ_DATA | {'maxPower', 'regen'}

    def __init__(self, equip_data):
        Equipment.__init__(self, equip_data)
        self._power = self.getMaxPower()
        self._hits = []

    def getPower(self):
        return self._power

    def getMaxPower(self):
        return self._data.maxPower

    def getRegen(self):
        return self._data.regen

    def atMax(self):
        return self._power == self.getMaxPower()

    def isDown(self):
        return self._power == 0

    def isHit(self):
        return len(self._hits) > 0

    def reportHits(self):
        hits = self._hits
        self._hits = []
        return hits

    def _update(self, dt):
        self._power += self.getRegen()
        if self._power > self.getMaxPower():
            self._power = self.getMaxPower()

        #Clear any past hits
        #self._hits = []

    def _hit(self, _dir, dmg):
        actual_dmg = 0
        self._power -= dmg
        if self._power < 0:
            actual_dmg = -self._power
            self._power = 0
        self._hits.append(_dir)
        return actual_dmg

#end Shield

class MiningLaser(AuxEquipment):

    TYPE = Type.TOOL
    REQ_DATA = Equipment.REQ_DATA | {'power', 'range'}

    def __init__(self, equip_data):
        Equipment.__init__(self, equip_data)
        self._target = None
        self._reload = True

    def getPower(self):
        return self._data.power

    def getRange(self):
        return self._data.range

    def setTarget(self, target_id):
        if hasattr(target_id, "id"):
            target_id = target_id.id
        self._target = target_id

    def getTarget(self):
        return self._target

    def canMine(self):
        return self._reload

    def mine(self, target_id = None):
        if not self.canMine():
            return False

        if not target_id:
            target_id = self._target

        if target_id:
            sm = self._ship._getMgr()
            return sm.mineEntity(target_id, self._ship.id, self.getPower())
        return False

    def _update(self, dt):
        self._reload = True
#end MiningLaser


class ShipFactory(AuxEquipment):
    from target import TargetPosition

    TYPE = Type.FACTORY

    def __init__(self, equip_data):
        Equipment.__init__(self, equip_data)
        self._cvector = primitives.Vector(-100, 100)
        self._attrs = None

    def setShipTemplate(self, attrs):
        self._attrs = attrs

    def constructShip(self, script_name):
        if not self._attrs:
            return None

        sm = self._ship._getMgr()
        ship_id, ship = sm.createShip(self._ship.owner, self._attrs)

        ship.setPosition(self._ship.pos.copy())
        ship.setTarget(self.TargetPosition(ship.pos + self._cvector))
        ship.setActiveScript(script_name)

        return ship_id
#end ShipFactory


class EquipmentManager(object):
    implements(IManager)

    EQUIP_BY_TYPE = {Type.ENGINE : Engine,
                     Type.RADAR : Radar,
                     Type.COMM : Communication,
                     Type.WEAPON : Weapon,
                     Type.SHIELD : Shield,
                     Type.TOOL : MiningLaser,
                     Type.FACTORY : ShipFactory}

    def __init__(self):
        self.equip_by_id = {}
        self.equip_by_name = {}

    def loadDatabase(self, equip_file):
        from csv import DictReader

        with open(equip_file, 'rb') as equip_db:

            curr_type = None
            equip_keys = []
            num_total = 0
            num_type = 0

            for line in equip_db:
                line = line.strip()
                if len(line) == 0:
                    continue
                log.debug(line)

                # look for custom CSV format line
                #+<equip_type>.<comma, seperated, keys>
                if line[0] == '+':
                    if curr_type:
                        log.info("Loaded %d of Type <%s>", num_type, curr_type)
                        num_type = 0

                    curr_type, csv_fields = line[1:].split('.')
                    equip_keys = csv_fields.split(',')
                    log.info("Type.%s %s", curr_type, str(equip_keys))
                    continue

                data = self._readEquipData(line, equip_keys)
                if data:
                    log.debug(str(data))

                    ed = EquipmentData(curr_type, **data)
                    self.equip_by_id[ed.id] = ed
                    self.equip_by_name[ed.name] = ed

                    num_type += 1
                    num_total += 1

            log.info("==== Loaded %d Total Equipment ====", num_total)

    def createEquipByName(self, name):
        ed = self.equip_by_name.get(name)
        if not ed:
            return None
        return self._createEquip(ed)

    def createEquipById(self, id_):
        ed = self.equip_by_id.get(id_)
        if not ed:
            return None
        return self._createEquip(ed)

    def _createEquip(self, ed):
        cls = self.EQUIP_BY_TYPE.get(ed.type)
        if not cls:
            return None
        return cls(ed)

    def _readEquipData(self, line, equip_keys):
        vals = line.split(',')
        if len(vals) != len(equip_keys):
            log.error("Invalid Equipment Data: %s", line)
            return None

        equip_dict = {}
        for key, val in zip(equip_keys, vals):
            # attempt to convert int/floats
            try:
                val = int(val)
            except ValueError:
                try:
                    val = float(val)
                except ValueError:
                    val = val.strip('"')

            equip_dict[key] = val

        return equip_dict


#end EquipmentManager