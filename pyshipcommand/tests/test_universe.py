from pyshipcommand.universe import Universe
from djlib.physics import Entity
from djlib.primitives import Vector

def test_universe_check_range():
    """Add some entities and check that the range checking functions work"""
    u = Universe()

    [u.addDynamic(entity) for entity in (Entity(Vector(0,0)),
                                          Entity(Vector(10, 10)),
                                          Entity(Vector(100, 100)))]

    assert len(u.getAllInRange(Vector(0,0), 25)) == 2





