
from djlib.primitives import Vector

from logging import getLogger
log = getLogger('pyshipcommand.target')


class TargetBase(object):
    
    def bind(self, src):
        """Bind to a source object to aid in Targeting"""
        self.src = src
        
    def getPosition(self):
        """Return the perceived position of this Target"""
        return self.src.getPosition()
        
    def getPower(self):
        """Return the relative power of the thrusters from 0.0 to 1.0"""
        return 1.0
        
    def atTarget(self):
        """Return whether pos satisfies the target"""
        return True
        
#end Target

class TargetPosition(TargetBase):
    
    DIST_THRESH = 1.0
    
    def __init__(self, pos):
        self.pos = pos
    
    def getPosition(self):
        return self.pos
    
    def atTarget(self):
        return (self.src.getPosition() - self.pos).length() < self.DIST_THRESH
    
#end TargetPosition


class RecursiveTarget(TargetBase):
    
    def __init__(self, target):
        assert(target.getPosition != None)
        self.target = target
        
    def bind(self, src):
        TargetBase.bind(self, src)
        
        if isinstance(self.target, TargetBase):
            self.target.bind(src)

#end RecursiveTarget


class TargetOrbit(RecursiveTarget):
    
    from math import pi, cos, sin, atan2
    ORBIT_STEP_RATIO = 10.0
    
    def __init__(self, center, radius):
        RecursiveTarget.__init__(self, center)
        self.radius = radius

        self.angle = 0.0
        num_steps = radius/self.ORBIT_STEP_RATIO
        if num_steps < 4.0:
            num_steps = 4.0
        self.angle_step = (2.0*self.pi)/num_steps
        
        self.pos = self._calcPos()

    def bind(self, src):
        self.src = src

        # Calculate nearest starting angle
        d = self.src.getPosition() - self.target.getPosition()
        self.angle = self.atan2(d.y, d.x)
        self.pos = self._calcPos()
    
    def getPosition(self):
        return self.pos
    
    def atTarget(self):
        
        # If ship reaches our position, calc next one
        if self.src.getPosition() == self.pos:
            self.angle += self.angle_step
            # XXX TODO: math is correct but implementation is slightly off
            #           tiny corner case here, if angle was 359 degrees and
            #           we rotate by 90 degrees, we end up at 449 degrees
            #           which is then changed to 0 instead of being set to
            #           89 degrees which would ratain the correct heading
            #           ie when rotating clockwise as we approch and exceed 
            #           360 degrees our rotation gets clipped and will most 
            #           likley present itself as non fluid clockwise motion
            #           
            #           side note: what happens if we rotate anti clockwise
            #                      and is that actually important ;)
            if self.angle > 2.0*self.pi:
                self.angle = 0.0
                
            self.pos = self._calcPos()
            log.debug("Reached Orbit Target - %s, updating angle(%.2f)", self.pos, self.angle)
        
        # return False no matter what to keep ship moving
        return False
    
    def _calcPos(self):
        vec = Vector(self.cos(self.angle), self.sin(self.angle))
        return self.target.getPosition() + (vec * self.radius)
    
#end TargetOrbit


class TargetFollow(RecursiveTarget):
    
    def __init__(self, target, dist):
        RecursiveTarget.__init__(self, target)
        self.dist = dist
        
    def getPosition(self):
        # project a target position at 'dist' from target
        d = self.src.getPosition() - self.target.getPosition()
        if d.length() > self.dist:
            return self.target.getPosition() + (d.normalized() * self.dist)
        return self.src.getPosition()
        
    def atTarget(self):
        d = self.target.getPosition() - self.src.getPosition()
        if d.length() > self.dist:
            return False
        return True
    
#end TargetFollow
