
from zope.interface import implements
from djlib.utils import IManager

from djlib.physics import Entity
from djlib.primitives import Vector, Rect, Circle
from djlib.spatial import ExpandingRectTree
import random

from djlib.logger import getLogger, log_method
log = getLogger('pyshipcommand.universe')

import celestial

# Universe generation constants
ASTEROIDS_PER_SECTOR = (100, 500)
SUNS_PER_SECTOR = (10, 30)
PLANETS_PER_SUN = (2, 5)
PLANET_ORBIT_DIST = (300, 500)



INITIAL_SIZE = 10000
EMPIRE_EMPIRE_DIST = 1000
EMPIRE_CELEST_DIST = 200



class Universe(object):
    implements(IManager)

    def __init__(self):
    	
    	# Initialize QuadTree
    	halfSize = INITIAL_SIZE/2
    	self.dyn_tree = ExpandingRectTree(Rect.fromSides(-halfSize, -halfSize, halfSize, halfSize))
        self.static_tree = ExpandingRectTree(Rect.fromSides(-halfSize, -halfSize, halfSize, halfSize))
        self.entity_by_id = {}

    @log_method(log)
    def load(self, filename):
    	log.info("Loading Universe from %s", filename)


    @log_method(log)
    def addDynamic(self, entity):
    	if not isinstance(entity, Entity):
    		raise TypeError()

        self.entity_by_id[entity.id] = entity
    	return self.dyn_tree.insert(entity)


    @log_method(log)
    def addStatic(self, entity):
        if not isinstance(entity, Entity):
            return TypeError()

        self.entity_by_id[entity.id] = entity
        return self.static_tree.insert(entity)


    @log_method(log)
    def getDynamicInRange(self, pos, dist):
        # rough entity lookup through the RectTree
        bounds = Rect.fromSides(pos.x-dist, pos.y-dist, pos.x+dist, pos.y+dist)
        rough = self.dyn_tree.getData(bounds)

        return [entity for entity in rough if pos.distanceApart(entity.getPosition()) <= dist]


    @log_method(log)
    def getStaticInRange(self, pos, dist):
        # rough entity lookup through the RectTree
        bounds = Rect.fromSides(pos.x-dist, pos.y-dist, pos.x+dist, pos.y+dist)
        rough = self.static_tree.getData(bounds)

        return [entity for entity in rough if pos.distanceApart(entity.getPosition()) <= dist]


    @log_method(log)
    def getAllInRange(self, pos, dist, type_ = None):

        entities = []
        if not type_ or "Ship" in type_:
            entities = self.getDynamicInRange(pos, dist)

        statics = self.getStaticInRange(pos, dist)
        if type_:
            statics = [entity for entity in statics if entity.__class__.__name__ in type_]
        entities.extend(statics)

        return entities


    @log_method(log)
    def getAllStatic(self):
        return self.static_tree.getData()


    @log_method(log)
    def getById(self, id):
        return self.entity_by_id.get(id)


    @log_method(log)
    def findNewEmpire(self):

        pos = None
        bounds = self.dyn_tree.rect.sides()
        while True:
            pos = Vector(random.randrange(bounds[0], bounds[2], EMPIRE_CELEST_DIST),
                        random.randrange(bounds[1], bounds[3], EMPIRE_CELEST_DIST))
            if len(self.getDynamicInRange(pos, EMPIRE_EMPIRE_DIST)) == 0 and \
                len(self.getStaticInRange(pos, EMPIRE_CELEST_DIST)) == 0:
                break

        return pos


    @log_method(log)
    def theBigBang(self):
    	
        # Only called during initialization.
        bounds = self.static_tree.rect.sides()

        # Randomize celestial count
        numSuns = random.randrange(*SUNS_PER_SECTOR)
        numAsteroids = random.randrange(*ASTEROIDS_PER_SECTOR)
        totPlanets = 0

        # Generate Suns
        for s in xrange(numSuns):

            pos = Vector(random.randrange(bounds[0], bounds[2]),
                         random.randrange(bounds[1], bounds[3]))
            sun = celestial.Sun(pos)
            self.addStatic(sun)

            # Generate Planets
            numPlanets = random.randrange(*PLANETS_PER_SUN)
            r = 0
            for p in xrange(numPlanets):
                r += random.randrange(*PLANET_ORBIT_DIST)
                orbit = Circle(sun.pos.copy(), r)
                self.addStatic(celestial.Planet(orbit))

            totPlanets += numPlanets
        #end Generate Suns

        # Generate Asteroids
        for a in xrange(numAsteroids):
            pos = Vector(random.randrange(bounds[0], bounds[2]),
                         random.randrange(bounds[1], bounds[3]))
            self.addStatic(celestial.Asteroid(pos))

        log.info("The Big Bang generated %d Suns, %d Planets, %d Asteroids", numSuns, totPlanets, numAsteroids)
   
#end Universe
