from logging import getLogger
log = getLogger('pyshipcommand.resources')


class Type(object):
	METAL = "metal"
	GAS = "gas"
	CRYSTAL = "crystal"
#end Type

class Resource(object):

	def __init__(self, _type, amount):
		self._type = _type
		self._amount = amount

	def getType(self):
		return self._type

	def getAmount(self):
		return self._amount

	def merge(self, res):
		if res._type != self._type:
			raise ValueError()
		self._amount += res._amount
		res._amount = 0

	def split(self, amount):
		if amount >= self._amount:
			raise ValueError()
		self._amount -= amount
		return Resource(self._type, amount)

	def __add__(self, res):
		if res._type != self._type:
			raise ValueError()
		amount = self._amount + res._amount
		self._amount, res._amount = (0,0)
		return Resource(self._type, amount)

	def __iadd__(self, res):
		self.merge(res)

	def __sub__(self, amount):
		return self.split(amount)

	def __repr__(self):
		return "%s(%d)" % (self._type, self._amount)
#end Resource

class ResourceSet(object):

	def __init__(self):
		self._resources = {}

	def getTypes(self):
		return self._resource.keys()

	def getAmount(self, _type = None):
		if not _type:
			return self.getTotal()

		r = self._resources.get(_type)
		if r is None:
			return 0
		return r._amount

	def getTotal(self):
		if len(self._resources) > 0:
			return sum([r._amount for r in self._resources.itervalues()])
		return 0

	def addResource(self, resource):
		r = self._resources.get(resource._type)
		if r:
			r.merge(resource)
			return

		self._resources[resource._type] = Resource(resource._type, resource._amount)
		resource._amount = 0

	def addSet(self, rset):
		for r in rset:
			self.addResource(r)

	def __add__(self, res_set):
		r = ResourceSet()
		r += self
		r += res_set
		return rset

	def __iadd__(self, res_set):
		if isinstance(res_set, Resource):
			self.addResource(res_set)
		elif isinstance(res_set, ResourceSet):
			self.addSet(res_set)
		else:
			raise TypeError()

	def __repr__(self):
		return "{%s}" % ",".join([str(r) for r in self._resources.itervalues()])

	def __iter__(self):
		return self._resources.itervalues()
#end ResourceSet




