
from zope.interface import implements
from zope.component import getUtility
from twisted.spread import pb
from twisted.cred import portal, checkers

from djlib.utils import IManager, IGame, IdAllocator
from ship import ShipAttributes

from network import NetVector, NetShip, NetInit, NetScript, NetCelestialBody
pb.setUnjellyableForClass(NetVector, NetVector)

from djlib.logger import getLogger, log_method
log = getLogger('pyshipcommand.players')

class Registrar(pb.Avatar):

    def __init__(self, remote):
        self.remote = remote

    @log_method(log)
    def perspective_registerAccount(self, name, password):
        pm = getUtility(IManager, "Player")

        if pm.getPlayer(name):
            self.remotePrint('Account name "%s" already exists' % name)
            return

        server = getUtility(IGame, "Game")
        server.addAccount(name, password)

#end Anon

class Player(pb.Avatar):

    def __init__(self, player_id, name):
	
        # members
        self.id = player_id
        self.name = name
        
        self.scripts = None
        self.resources = None
        self.remote = None

        uni = getUtility(IManager, "Universe")
        empire_pos = uni.findNewEmpire()

        sm = getUtility(IManager, "Ship")
        self.mship = sm.createMothership(self.name, empire_pos)


    @log_method(log)		
    def connect(self, mind):

        if self.isConnected():
            # Just update the mind and leave
            self.remote = mind
            return False
    
        log.info('Player: "%s" has connected', self.name)
        self.remote = mind

        return True


    @log_method(log)            
    def disconnect(self, mind):
        if self.remote == mind:
            self.remote = None
            log.info('Player: "%s" has disconnected', self.name)
            return True
        return False


    @log_method(log)
    def remotePrint(self, msg):
        log.info('Sending message to Player: "%s" - %s', self.name, msg)
        if self.remote:
            self.remote.callRemote("print", msg)


    @log_method(log)
    def sendShips(self):

        # player might be disconnected
        if not self.isConnected():
            return

        ships = self.getShips()
        if not ships:
            return
            
        # convert only updated ships
        net_ships = []
        for ship in ships:
            net_ships.append(NetShip(ship))
            
        self.remote.callRemote("updateShips", net_ships)
        log.info("%s: Sent %d ships to %s", self.name, len(net_ships), self)

        self.sendLog()


    @log_method(log)
    def sendUniverse(self):
        if not self.isConnected():
            return

        univ = getUtility(IManager, "Universe")
        bodies = univ.getAllStatic()

        net_bodies = [NetCelestialBody(b) for b in bodies]
        self.remote.callRemote("updateUniverse", net_bodies) 


    @log_method(log)
    def sendLog(self):

        if not self.isConnected():
            return

        sm = getUtility(IManager, "Ship")
        ship_log = sm.getShipLog(self.name, True)

        if len(ship_log) > 0:
            self.remote.callRemote("log", ship_log)
        log.info("%s: Sending ship log of length %d to %s", self.name, len(ship_log), self)


    @log_method(log)
    def showText(self, text):
        if not self.isConnected():
            return

        log.info("%s: Showing text to player (showText) - %s", self.name, text)
        self.remote.callRemote("showText", text)


    @log_method(log)
    def getShips(self):
        sm = getUtility(IManager, "Ship")
        return sm.getPlayerShips(self.name)


    @log_method(log)
    def isConnected(self):
        return self.remote is not None
        

    # REMOTE CALLS
    ###################################

    @log_method(log)
    def perspective_info(self):
        self.remotePrint("%s(%d)" % (self.name, self.id))


    @log_method(log)
    def perspective_giveScript(self, name, data):
        log.info("%s: Receiving script %s", self.name, name)
        sm = getUtility(IManager, "Script")
        sm.giveScript(self.name, name, data)


    @log_method(log)
    def perspective_giveMessage(self, msg):
        log.info("%s: Receiving message: %s", self.name, str(msg))
        sm = getUtility(IManager, "Ship")
        mship = sm.getMothership(self.name)
        if sm.sendMessage(None, mship.id, 0, msg):
            self.remotePrint("Message Success")
        else:
            self.remotePrint("Message Failed")


    @log_method(log)
    def perspective_scriptShip(self, ship_id, script_name, action):
        sm = getUtility(IManager, "Ship")
        scm = getUtility(IManager, "Script")
        ship = sm.getShip(ship_id)
        if not ship or not ship.setActiveScript(script_name, action == "reboot"):
            self.remotePrint("Ship ID or Script invalid")


    @log_method(log)
    def perspective_getClientInit(self):
        from server import SERVER_UPDATE_INT

        sm = getUtility(IManager, "Ship")
        mship = sm.getMothership(self.name)

        # construct client init data
        cinit = NetInit()
        cinit.player_id = self.id
        cinit.player = self.name
        cinit.start_pos = NetVector(*mship.getPosition().args())
        cinit.mship_id = mship.id
        cinit.server_step = SERVER_UPDATE_INT
        cinit.scripts = []

        scm = getUtility(IManager, "Script")
        scripts = scm._getPlayerScripts(self.name)
        if scripts:
            cinit.scripts = [NetScript(s.name, s.version, s.lastHash) for s in scripts.itervalues()]

        # Send Universe
        univ = getUtility(IManager, "Universe")
        bodies = univ.getAllStatic()
        cinit.universe = [NetCelestialBody(b) for b in bodies]

        # Send Ships
        ships = self.getShips()
        cinit.ships = [NetShip(s) for s in ships]
        
        return cinit


    @log_method(log)
    def perspective_giveChat(self, msg):
        # Prepend player name
        msg = "%s: %s" % (self.name, msg)
        pm = getUtility(IManager, "Player")

        # Don't mute ourselves so we can see chat history
        pm.broadcastMessage(None, msg)


    @log_method(log)
    def perspective_getPlayers(self):

        pm = getUtility(IManager, "Player")
        pstr = ["Connected Players:"]
        for p in pm.connectedPlayers:
            pstr.append(p.name)
        self.showText(pstr)

# end Player


class AdminPlayer(Player):

    @log_method(log)
    def getShips(self):
        #AdminPlayers get sent all ships
        sm = getUtility(IManager, "Ship")
        return sm.ships_by_id.values()


    # REMOTE CALLS
    ###################################

    @log_method(log)
    def perspective_addShip(self, pos):
        sm = getUtility(IManager, "Ship")
        sid, s = sm.createShip(self.name, ShipAttributes())
        s.setPosition(pos)
        log.info("Added Ship[%s] @ %s", sid, pos)
        self.remotePrint(str(s))


    @log_method(log)
    def perspective_moveShip(self, ship_id, target_pos):
        from target import TargetPosition
        sm = getUtility(IManager, "Ship")
        ship = sm.getShip(ship_id)
        if not ship:
            self.remotePrint("Ship[%s] does not exist" % ship_id)
            return
        
        self.remotePrint("Moving Ship[%s] to %s" % (ship_id, target_pos))
        ship.setTarget(TargetPosition(target_pos))


    @log_method(log)
    def perspective_orbitShip(self, ship_id, target_pos, radius):
        from target import TargetPosition, TargetOrbit
        sm = getUtility(IManager, "Ship")
        ship = sm.getShip(ship_id)
        if not ship:
            self.remotePrint("Ship[%s] does not exist" % ship_id)
            return
        
        self.remotePrint("Orbiting Ship[%s] @ %s" % (ship_id, target_pos))
        pos = TargetPosition(target_pos)
        t_ship = sm.getShip(int(target_pos.x))
        if int(target_pos.y) == 0 and t_ship:
            # What are the odds that Y would be 0 and X also match a ship ID?
            # Should probably changes this ;)
            log.debug("Ship[%s] is orbiting Ship[%s]", ship_id, t_ship)
            pos = t_ship
        target = TargetOrbit(pos, radius)
        ship.setTarget(target)


    @log_method(log)        
    def perspective_followShip(self, ship_id, target_id, dist):
        from target import TargetFollow
        sm = getUtility(IManager, "Ship")
        ship = sm.getShip(ship_id)
        target = sm.getShip(target_id)
        if not ship or not target:
            self.remotePrint("Ship ID or Target ID invalid")
            return
        
        self.remotePrint("Ship[%d] now following Ship[%d] at %d" % (ship_id, target_id, dist))
        ship.setTarget(TargetFollow(target, dist))


    @log_method(log)
    def perspective_attackShip(self, ship_id, target_id, dmg):
        from target import TargetOrbit
        sm = getUtility(IManager, "Ship")
        ship = sm.getShip(ship_id)
        target = sm.getShip(target_id)
        if not ship or not target:
            self.remotePrint("Ship ID or Target ID invalid")
            return

        ship.shoot(target_id, dmg, 100000)


    @log_method(log)
    def perspective_getPlayers(self):
        #Overidden for Admins to provide more information
        pm = getUtility(IManager, "Player")
        pstr = ["Connected Players:"]
        for p in pm.connectedPlayers:
            pstr.append("%s[%d] %s" % (p.name, p.id, str(p.mship.getPosition())))
        self.showText(pstr)
                         
#end AdminPlayer

PLAYER_ID_START = 1234

class PlayerManager(object):
    implements(IManager, portal.IRealm)

    def __init__(self):
        self.playersName = {}
        self.playersId = {}
        self.id_gen = IdAllocator(PLAYER_ID_START)

        self.connectedPlayers = []


    @log_method(log)
    def requestAvatar(self, avatarId, mind, *interfaces):
        assert pb.IPerspective in interfaces

        # Allow anonymous access for account registration
        if avatarId == "register":
            # XXX TODO: fill this in with more contextual info, new player name
            #           and new player id for accounting purposes
            log.info('Creating new account')
            return pb.IPerspective, Registrar(mind), lambda:None

        player = self.createPlayer(avatarId)
        self.connectAvatar(player, mind)
        
        return pb.IPerspective, player, lambda p=self:p.disconnectAvatar(player, mind)


    @log_method(log)
    def connectAvatar(self, player, mind):
        if player.connect(mind):
            self.broadcastMessage(player, "%s has joined the Universe" % (player.name))
            self.connectedPlayers.append(player)


    @log_method(log)
    def disconnectAvatar(self, player, mind):
        if player.disconnect(mind):
            self.connectedPlayers.remove(player)
            self.broadcastMessage(player, "%s has left the Universe" % (player.name))


    @log_method(log)
    def getPlayer(self, identifier):
        if isinstance(identifier, int):
            return self.playersId.get(identifier)
        elif isinstance(identifier, str):
            return self.playersName.get(identifier)
        return None


    @log_method(log)
    def createPlayer(self, name):

        # Check if player already exists
        player = self.getPlayer(name)
        if player:
            return player

        # Actually create a new player
        player_id = self.id_gen.allocate()
        assert player_id
        player = None
        if name == "admin":
            player = AdminPlayer(player_id, name)
            log.info("Creating Admin account, ID: %s", player_id)
        else:
            # XXX TODO: check if this should be a diffrent type of Player for non admin players
            player = AdminPlayer(player_id, name)
            log.info("Creating Player account, ID: %s", player_id)

        # Add player to tracking maps
        self.playersName[name] = player
        self.playersId[player_id] = player

        return player


    @log_method(log)
    def loadPlayers(self, player_file):
        log.info("Loading Players:")
        players = []
        
        for player in players:
            log.info("Loaded Player %s: %s", player.id, player.name)


    @log_method(log)
    def broadcastMessage(self, muted_player, msg):
        for p in self.connectedPlayers:
            if not p == muted_player:
                p.showText(msg)

#end PlayerManager
		
