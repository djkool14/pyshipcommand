
from zope.interface import implements
from zope.component import getUtility
from djlib.utils import IManager

from djlib import physics
from djlib.primitives import Vector
from djlib.utils import IAllocator
from target import TargetBase

import equipment
from resources import Resource, ResourceSet

from logging import getLogger
log = getLogger('pyshipcommand.ship')


class ShipAttributes(object):
    def __init__(self):
        self.cargo_cap = 1000
        self.aux_equip_slots = 1
        self.hull_strength = 10
        self.crew_size = 1

    def __str__(self):
        return '\n'.join(["Cargo Capacity : %i"%self.cargo_cap,
                          "Aux Equipment Slots : %i"%self.aux_equip_slots,
                          "Hull Strength : %i"%self.hull_strength,
                          "Crew Size : %i"%self.crew_size])
# end ShipAttributes


class Ship(physics.PhysicsEntity):

    # Empty data object for storing persistance script information
    class Data:
        def __init__(self):
            self.initialized = False
            self.last_script = ("", 0)
    #end Data


    def __init__(self, player, ship_id, ship_attribs, phys_attribs, position = Vector(0,0)):
        physics.PhysicsEntity.__init__(self, position, phys_attribs)
        self.owner = player
        self.script = None
        self.namespace = None
        self.id = ship_id
        self.attrs = ship_attribs
        self.std_equip = {}
        self.aux_equip = {}
        self.health = ship_attribs.hull_strength
        self.recent_attack = None
        self.msgs = None

        self.cargo = ResourceSet()

        self.heading = Vector(0.0, 1.0)
        self.target = None

        self.data = Ship.Data()

        # add ourselves to the universe
        universe = getUtility(IManager, "Universe")
        self._node = universe.addDynamic(self)

    def update(self, time_step):

        # target determines ship destination
        if self.target:

            if not self.target.atTarget():
                # Calculate heading
                target_pos = self.target.getPosition()
                d = (target_pos-self.pos)
                
                self.setHeading(d.normalized(), self.target.getPower())
            
                # Cheat if we are getting close
                if d.length() <= self.vel.length() * time_step:
                    self.setVelocity(d.scaled(1.0/time_step))
                    
            else: #atTarget
                self.stop()
        
        # still update the physical entity for basic movement
        self._node.remove(self)
        physics.PhysicsEntity.update(self, time_step)

        universe = getUtility(IManager, "Universe")
        self._node = universe.addDynamic(self)

        # Update all of our equipment
        for equip in self.std_equip.values():
            equip._update(time_step)
        for equip in self.aux_equip.values():
            equip._update(time_step)

        # reset recent_attack
        self.recent_attack = None

    def setPosition(self, pos):
        universe = getUtility(IManager, "Universe")

        self._node.remove(self)
        physics.PhysicsEntity.setPosition(self, pos)
        self._node = universe.addDynamic(self)

    def addAuxEquipment(self, equip):
        # type checks
        if not isinstance(equip, equipment.AuxEquipment):
            raise TypeError()

        if len(self.aux_equip) >= self.attrs.aux_equip_slots:
            return False

        self.aux_equip[equip.getType()] = equip
        equip._install(self)
        return True

    def upgradeEquipment(self, equip):
        if not isinstance(equip, equipment.BaseEquipment):
            raise TypeError()

        self.std_equip[equip.getType()] = equip
        equip._install(self)

    def isAlive(self):
        return self.health >= 0

    def isEnemy(self, other_ship):
        return self.owner is not other_ship.owner

    def setTarget(self, target):
        if not target:
            self.target = None
            return

        if isinstance(target, TargetBase):
            target.bind(self)
            self.target = target

    def atTarget(self):
        if self.target:
            return self.target.atTarget()
        return True

    def setHeading(self, dir, power):
        speed = power * self.phys_attrs.max_vel
        self.heading = dir
        self.setVelocity(dir * speed)
        
    def stop(self):
        self.vel.clear()
        
    def getPower(self):
        self.vel.length()/self.phys_attrs.max_vel
        
    def getData(self):
        return self.data

    def log(self, msg):
        ship_log = self._getMgr().getShipLog(self.owner)
        ship_log.append("%s[%d]:%s" % (self.script.name if self.script else "", self.id, msg))

    def getEntityInRange(self, dist, type_ = None):
        sm = self._getMgr()
        return sm.getEntityInRange(self, dist, type_)

    def shoot(self, target_id, dmg, max_range):
        sm = self._getMgr()
        r = sm.attackShip(self, target_id, dmg, max_range)

        if r:
            self.recent_attack = target_id
        return r

    def sendMessage(self, target_id, max_range, msg):
        return self._getMgr().sendMessage(self, target_id, max_range, msg)

    def giveMessage(self, source_id, msg):
        if self.msgs:
            self.msgs.append((source_id, msg))
            return
        self.msgs = [(source_id, msg)]

    def getContainer(self):
        # The container in this case is the ship's cargo.
        return self.cargo

    def giveCargo(self, rset):
        if self.cargo.getAmount()+rset.getAmount() <= self.attrs.cargo_cap:
            self.cargo += rset
            return True
        return False

    def _getMessages(self):
        msgs = self.msgs
        self.msgs = None
        return msgs

    def _damage(self, _dir, dmg):

        shield = self.getEquipment(equipment.Type.SHIELD)
        if shield:
            dmg = shield._hit(_dir, dmg)

        self.health -= dmg
        return self.health

    def getEquipment(self, type_):
        e = self.std_equip.get(type_, None)
        if not e:
            return self.aux_equip.get(type_, None)
        return e
        
    def setActiveScript(self, script_name, reboot = False):
        scm = getUtility(IManager, "Script")
        script = scm.getScript(self.owner, script_name)

        if script:
            # Store previous script info
            if self.script:
                self.data.last_script = (self.script.name, self.script.version)
                
            # Allocate new script namespace
            self.script = script
            self.namespace = scm.createNamespace(self.owner)

            # Inject this ship into the namespace
            self.namespace["ship"] = SafeShip(self)
            self.data.initialized = False

            if reboot:
                # Wipe ships data. This will also remove the last_script
                # reference set above, but that is only used for upgrades anyway.
                self.data = Ship.Data()

            return True
        return False
        
    def runScript(self):
        if self.script:
            try:
                self.script.run(self.namespace)
                return True
            except Exception as e:
                logMsg  = "\n************ SCRIPT EXCEPTION **************\n"
                logMsg += str(e)
                logMsg += "\n********************************************\n"
                self.log(logMsg)
        return False

    def isMothership(self):
        return self._getMgr().getMothership(self.owner) is self

    def _destroyed(self):
        self._node.remove(self)

    def _getMgr(self):
        return getUtility(IManager, "Ship")

    def __str__(self):
        return "Ship(%s[%d])"%(self.owner, self.id)

#end Ship

from copy import copy
class SafeShip(object):

    COPY_LIST = ( "owner", "id", "attrs", "heading", "health", "pos")
    WHITE_LIST = ( "isEnemy", "setTarget", "atTarget", "setHeading", "stop", "getPower", "getData", "log", "setActiveScript", "getEquipment", "isAlive", "target")

    def __init__(self, ship):
        self._ship = ship

    def __getattr__(self, name):
        if name in SafeShip.COPY_LIST:
            attr = getattr(self._ship, name)
            return copy(attr) if attr else None
        if name in SafeShip.WHITE_LIST:
            return getattr(self._ship, name)

        raise AttributeError()

    def getEntityInRange(self, dist = 0, type_ = None):
        return self._ship.getEntityInRange(dist, type_)
#end SafeShip


import time
class ShipMgr(object):
    implements(IManager)

    def __init__(self):
        self.ships_by_player = {}
        self.log_by_player = {}
        self.ships_by_id = {}
        self.queued_msgs = []

    def update(self, time_step):

        # update all ships given the time_step
        t = time.clock()
        for ship in self.ships_by_id.itervalues():
            ship.update(time_step)
        log.info("Updated %d ship(s) in %.3f seconds", len(self.ships_by_id), time.clock()-t)
        
        # Now run all ship scripts, this could possibly modify the ships dict,
        # use a copy of the ships instead

        c = 0
        # XXX TODO: i hope you read the manual for this one, specifically the 'window/unix' diffrences
        t = time.clock()
        for ship in self.ships_by_id.values():
            if ship.runScript():
                c += 1
        log.info("Updated %d scripts(s) in %.3f seconds", c, time.clock()-t)

        # Distribute all messages
        self._distributeMessages()

        # Destroy any ships that have been killed
        killed = []
        for ship in self.ships_by_id.itervalues():
            if not ship.isAlive():
                killed.append(ship)

        for ship in killed:
            self.destroyShip(ship)
        if len(killed) > 0:
            log.info("Destroyed %d ship(s)", len(killed))

            
    def createShip(self, player, ship_attribs):
        id_gen = getUtility(IAllocator, "IdGenerator")
        ship_id = id_gen.allocate()
        player_ships = self.getPlayerShips(player, True)

        # create and add ship to the various maps
        ship = Ship(player, ship_id, ship_attribs, self.calculatePhysicalAttributes(ship_attribs))

        self.ships_by_id[ship_id] = ship
        player_ships.append(ship)

        # install standard equipment
        #ship.upgradeEquipment(equipment.Communication("Standard Comm", 1, 1000, 256))
        #ship.upgradeEquipment(equipment.Radar("Standard Radar", 1, 400))
        #ship.upgradeEquipment(equipment.Weapon("Stardard Laser", 1, 5, 5.0, 200))
        #ship.addAuxEquipment(equipment.MiningLaser("Mining Laser", 1, 10, 200))
        em = getUtility(IManager, "Equipment")
        ##
        #standard equipment.
        ship.upgradeEquipment(em.createEquipByName("Standard Comm"))
        ship.upgradeEquipment(em.createEquipByName("Standard Radar"))
        ship.upgradeEquipment(em.createEquipByName("Stardard Laser"))
        ship.addAuxEquipment(em.createEquipByName("Mining Laser"))

        return (ship_id, ship)

    def createMothership(self, player, pos):

        # The players very first ship must be their mothership
        player_ships = self.getPlayerShips(player, True)
        assert(len(player_ships) == 0)

        # Create the ship with special attributes
        pa = physics.Attributes(1.0, 1000000.0, 100.0)
        sa = ShipAttributes()
        sa.cargo_cap = 10000
        sa.aux_equip_slots = 10
        sa.hull_strength = 1000

        id_gen = getUtility(IAllocator, "IdGenerator")
        ship_id = id_gen.allocate()
        mship = Ship(player, ship_id, sa, pa)

        self.ships_by_id[ship_id] = mship
        player_ships.append(mship)
        mship.setPosition(pos)

        em = getUtility(IManager, "Equipment")
        #mship.upgradeEquipment(equipment.Communication("Mega Comm", 2, 10000, 512))
        #mship.upgradeEquipment(equipment.Weapon("Mega Laser", 2, 20, 5.0, 400))
        #mship.addAuxEquipment(equipment.Shield("Mega Shield", 2, 15, 5))
        #mship.addAuxEquipment(equipment.ShipFactory(ShipAttributes()))
        ##
        #Standard Equipment
        mship.upgradeEquipment(em.createEquipByName("Mega Comm"))
        mship.upgradeEquipment(em.createEquipByName("Mega Laser"))
        mship.addAuxEquipment(em.createEquipByName("Mega Shield"))
        factory = em.createEquipByName("Ship Factory")
        factory.setShipTemplate(ShipAttributes())
        mship.addAuxEquipment(factory)

        return mship


    def destroyShip(self, ship):

        # Remove ship from all maps
        player_ships = self.getPlayerShips(ship.owner)
        player_ships.remove(ship)
        del self.ships_by_id[ship.id]

        # Let ship clean up anything else
        ship._destroyed()


    def getShip(self, ship_id):
        return self.ships_by_id.get(ship_id)

    def getShipsInRange(self, ship, range_):
        u = getUtility(IManager, "Universe")
        return [ship for ship in u.getAllInRange(ship.getPosition(), range_, "Ship") if ship.id != ship.id]

    def getEntityInRange(self, ship, range_, type_):
        u = getUtility(IManager, "Universe")
        return [entity for entity in u.getAllInRange(ship.getPosition(), range_, type_) if entity.id != ship.id]

    def attackShip(self, src_ship, target_id, dmg, max_range):

        # Check for existence of target
        target_ship = self.getShip(target_id)
        if not target_ship:
            return False

        # Make sure the source ship is in range. Allow for NoneType src_ship for omniscient attacks
        src_pos = src_ship.getPosition() if src_ship else target_ship.getPosition()
        d = target_ship.getPosition() - src_pos
        if d.length() > max_range:
            log.info("%s attacked %s but missed", src_ship, target_ship)
            return False

        log.info("%s attacked %s for %d dmg", src_ship, target_ship, dmg)

        target_ship._damage(d.normalized(), dmg)
        return True

    def mineEntity(self, src_id, ship_id, amount):
        dest_ship = self.getShip(ship_id)
        assert(dest_ship)
        dest_con = dest_ship.getContainer()

        uni = getUtility(IManager, "Universe")
        src_ent = uni.getById(src_id)
        if not src_ent:
            return False

        # Cap the amount to the remaining capacities
        cap = dest_ship.attrs.cargo_cap - dest_ship.cargo.getAmount()
        amount = cap if amount > cap else amount
        if amount <= 0:
            ship.log("Cargo Full")
            return False

        src_con = src_ent.getContainer()
        cap = src_con.getAmount()
        amount = cap if amount > cap else amount
        if amount <= 0:
            ship.log("Mining Target is empty")
            return False

        # Mining will result in resources based on the ratio within the entity
        total = src_con.getAmount()
        for r in src_con:
            ratio = r.getAmount()/total
            mined_res = r.split(ratio*amount)
            dest_ship.log("%s mined %s from %d" % (str(dest_ship), str(mined_res), src_id))
            dest_con.addResource(mined_res)

        return True

    def sendMessage(self, src_ship, target_id, max_range, msg):

        # Check for existence of target
        # TODO: Allow for broadcast messages
        target_ship = self.getShip(target_id)
        if not target_ship:
            return False

        # Make sure the source ship is in range.
        if src_ship and src_ship.getPosition().distanceApart(target_ship.getPosition()) > max_range:
            return False

        # Queue up message to be distributed later
        self.queued_msgs.append((src_ship, target_ship, msg))
        return True

    def _distributeMessages(self):
        for src, target, msg in self.queued_msgs:
            target.giveMessage(src.id if src else None, msg)
        self.queued_msgs = []
    
    def calculatePhysicalAttributes(self, ship_attribs):
        # for now, just return the default physical attributes
        return physics.Attributes()

    def getPlayerShips(self, player, create_player = False):
        player_ships = self.ships_by_player.get(player)
        if not create_player:
            return player_ships
        elif not player_ships:
            self.ships_by_player[player] = []
        return self.ships_by_player[player]

    def getShipLog(self, player, clear = False):
        ship_log = self.log_by_player.get(player)
        if not ship_log:
            ship_log = []
            self.log_by_player[player] = ship_log

        if clear:
            self.log_by_player[player] = []
        return ship_log
    
    def sendAllShips(self):
        player_mgr = getUtility(IManager, "Player")
        for player in player_mgr.playersId.itervalues():
            player.sendShips()

    def getPlayerFromShip(self, ship):
        for player, player_ships in self.ships_by_player.iteritems():
            if ship in player_ships:
                return player

    def getMothership(self, player):
        player_ships = self.getPlayerShips(player)
        if player_ships:
            return player_ships[0]
        return None

#end ShipMgr
