# Twisted Framework
from zope.component import getGlobalSiteManager
from zope.interface import implements
from twisted.internet import reactor, task
from twisted.cred import checkers, credentials, portal
from twisted.spread.pb import PBServerFactory
from twisted.internet.error import CannotListenError

# PyCommand
from scripts import ScriptMgr
from players import PlayerManager
from universe import Universe
from ship import ShipMgr
from equipment import EquipmentManager
from djlib.utils import IManager, IGame, IdAllocator, IAllocator

from djlib.logger import getLogger, log_method, add_log_args, set_log_options
log = getLogger('pyshipcommand.server')

from collections import namedtuple
# used in the function main() below as part of the default arguments
import sys

SERVER_EXIT_OK = 0
SERVER_EXIT_UNKNOWN = 1
SERVER_EXIT_ADDRESS_IN_USE = 2

# CONSTANTS
DEFAULT_INTERFACE = ""
DEFAULT_PORT = 12345
SERVER_UPDATE_INT = 10.0 #seconds

# CONSTANTS
SERVER_INDEX_MOD = 12345

class InitializeError(Exception): pass

Address = namedtuple('Address', 'interface port')

class ShipCommandServer(object):
    implements(IGame)
    
    def __init__(self, address=Address(DEFAULT_INTERFACE, DEFAULT_PORT)):
        self.address = address
        
        self.universe = Universe()
        self.player_mgr = PlayerManager()
        self.script_mgr = ScriptMgr()
        self.ship_mgr = ShipMgr()
        self.equip_mgr = EquipmentManager()
        self.id_gen = IdAllocator(SERVER_INDEX_MOD)
        self.accounts = None
        self.tick = 0

    @log_method(log)
    def initialize(self):
        log.info("Intializing Server")

        # Register global manager utilties
        gsm = getGlobalSiteManager()
        gsm.registerUtility(self.id_gen, IAllocator, "IdGenerator")
        gsm.registerUtility(self, IGame, "Game")
        gsm.registerUtility(self.universe, IManager, "Universe")
        gsm.registerUtility(self.player_mgr, IManager, "Player")
        gsm.registerUtility(self.script_mgr, IManager, "Script")
        gsm.registerUtility(self.ship_mgr, IManager, "Ship")
        gsm.registerUtility(self.equip_mgr, IManager, "Equipment")

        self.universe.theBigBang()
        self.player_mgr.loadPlayers("players.dat")
        self.script_mgr.loadAllScripts()
        #self.universe.loadWorld("")

        self.equip_mgr.loadDatabase("equip_db.dat")

        # Initialize Twisted authentication service
        p = portal.Portal(self.player_mgr)
        self.accounts = checkers.InMemoryUsernamePasswordDatabaseDontUse()
        self.accounts.addUser("register", "account")
        num_accounts = self.loadCredentials(self.accounts)
        log.info("Loaded %d Accounts", num_accounts)
        p.registerChecker(self.accounts)

        # Start listening for connections
        reactor.listenTCP(interface=self.address.interface,
                          port=self.address.port,
                          factory=PBServerFactory(p))
        log.info('Listening on %s:%d', self.address.interface or '*', self.address.port)

        return True

    @log_method(log)
    def update(self):
        # XXX TODO: Consider renaming this 'tick' as this is common in game terminology for this
        #           sort of function. it is a tick and a ticks function is to update so slightly
        #           more desciprtive and in line with expectation
        log.info("--------------------- Server Tick: %d ---------------------", self.tick)
        self.ship_mgr.update(SERVER_UPDATE_INT)
        self.ship_mgr.sendAllShips()
        self.tick += 1
        
    @log_method(log)
    def loadCredentials(self, checker):

        count = 0
        with open("creds.dat", "a+") as creds:
            creds.seek(0)
            for cred in creds:
                name, password = cred.strip().split(":")
                log.info('Loaded Player: "%s"', name)
                checker.addUser(name, password)
                self.player_mgr.createPlayer(name)
                count+=1

        return count

    @log_method(log)
    def addAccount(self, user, password):
        log.info("Adding account: %s", user)
        self.accounts.addUser(user, password)

        with open("creds.dat", "a+") as creds:
            creds.write("%s:%s\n"%(user, password))

    @log_method
    def shutdown(self):
        log.info("Shutting down Server")

        # Save all current state
        #self.universe.save()
        #self.player_mgr.save()
#end ShipCommandServer

def main(argv=sys.argv[1:]):
    """Main Entry point for the server program
    """
    from argparse import ArgumentParser, FileType
    
    # Argument parsing
    args = ArgumentParser()
    args.add_argument('-i', '--interface',
                      help="Interface to listen for new client connections")
    args.add_argument('-p', '--port', type=int,
                      help="Port to listen for new client connections")
    args.add_argument('-c', '--config', action='append', default=[],
                      help="The config file used to set up the server")

    # Add reusable logging options
    add_log_args(args)

    options = args.parse_args(argv)

    # Apply log options
    set_log_options(options, 'pyshipcommand')

    # Config file
    from ConfigParser import SafeConfigParser as ConfigParser, NoSectionError
    config = ConfigParser()
    filenames = config.read(options.config)
    log.info('Read %d config files: %s', len(filenames), ', '.join(filenames))
    
    # Detirmine the interface to listen on
    try:
        interface = options.interface or config.get('server', 'interface')
    except NoSectionError:
        interface = DEFAULT_INTERFACE
        
    try:
        port = options.port or config.get('server', 'port')
    except NoSectionError:
        port = DEFAULT_PORT
    
    address = Address(interface, port)
    
    # Server Creation
    server = ShipCommandServer(address)

    try:
        if not server.initialize():
            raise InitializeError()

        update_task = task.LoopingCall(server.update)
        update_task.start(SERVER_UPDATE_INT)
        reactor.run()

        server.shutdown()
    except CannotListenError:
        log.error("Address: %s:%d already in use", *address)
        return SERVER_EXIT_ADDRESS_IN_USE
    except InitializeError:
        log.error("Could not start server")
        return SERVER_EXIT_UNKNOWN
                
# Server entry point
if __name__ == "__main__":
    sys.exit(main())

# end __main__