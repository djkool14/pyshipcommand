
from random import randrange, uniform
from math import pi, pow

from zope.component import getUtility
from djlib.primitives import Circle
from djlib.utils import IAllocator
from resources import *

from logging import getLogger
log = getLogger('pyshipcommand.celestial')

PLANET_CRYSTAL_RATIO = 0.1


class CelestialBody(Circle):
	def __init__(self, pos, size):
		Circle.__init__(self, pos, size)
		id_gen = getUtility(IAllocator, "IdGenerator")
		self.id = id_gen.allocate()
		self._resources = self._genResources()

	def getContainer(self):
		return self._resources

	def _getVolume(self):
		return int((4.0/3.0)*pi*pow(self.radius, 3))

	def _genResources(self):
		return None

#end CelestialBody

class Sun(CelestialBody):
	def __init__(self, pos):
		CelestialBody.__init__(self, pos, randrange(50, 100))

	def _genResources(self):
		rset = ResourceSet()
		rset.addResource(Resource(Type.GAS, self._getVolume()))
		return rset
#end Sun

class Planet(CelestialBody):
	def __init__(self, orbit):
		pos = orbit.pointOnCircle(uniform(0.0, 2.0*pi))
		CelestialBody.__init__(self, pos, randrange(20, 40))

	def _genResources(self):
		rset = ResourceSet()
		c = randrange(0, int(self._getVolume()*PLANET_CRYSTAL_RATIO))
		rset.addResource(Resource(Type.CRYSTAL, c))
		rset.addResource(Resource(Type.METAL, self._getVolume()-c))
		return rset
#end Planet	

class Asteroid(CelestialBody):
	def __init__(self, pos):
		CelestialBody.__init__(self, pos, randrange(10, 20))

	def _genResources(self):
		rset = ResourceSet()
		rset.addResource(Resource(Type.METAL, self._getVolume()))
		return rset
#end Asteroid
