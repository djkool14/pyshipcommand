
from zope.interface import implements
from djlib.utils import IManager

import os
import sys
import glob
import hashlib
import imp
from traceback import format_exc

from RestrictedPython import compile_restricted
from RestrictedPython.Guards import safe_builtins
from RestrictedPython.PrintCollector import PrintCollector

from logging import getLogger
log = getLogger('pyshipcommand.scripts')

# CONSTANTS
SCRIPT_DIR = "player_scripts"

ALLOWED_IMPORTS = [ "djlib.primitives", "pyshipcommand.target", "random", "math" ]


# XXX TODO: The safe_builtins need to be modified to include functions
# we deem as safe. So far, the only builtins included are ones used
# in existing example scripts. More can be added as needed.
safe_builtins.update({"min":min,
                      "max":max,
                      "enumerate":enumerate})

def player_import(player_name, player_scripts):
    def safe_import(name, g = globals(), l = locals(), fromlist = [], level = -1):
        log.debug('%s: safe imported module "%s"', player_name, name)
        if name in ALLOWED_IMPORTS:
            return __import__(name, g, l, fromlist, level)

        script = player_scripts.get(name)
        if script:
            log.debug('%s: Importing player module', player_name)
            if not script.module:
                log.info('%s: constructing new module from script "%s"', player_name, script.name)
                script.module = imp.new_module(script.name)

                # use safe builtins
                d = script.module.__dict__
                d["__builtins__"] = safe_builtins
                d["_print_"] = PrintCollector
                d["__name__"] = "__module__"
                d["_write_"] = safe_write
                d["_getattr_"] = safe_getattr
                d["_setattr_"] = safe_setattr
                d["_getitem_"] = safe_getitem
                d["_getiter_"] = lambda self: self.__iter__()
                
                # reuse safe import
                d["__builtins__"]["__import__"] = safe_import
                exec script.compiled in script.module.__dict__
            return script.module

        return None
    return safe_import


# DEBUG Restricted Access
#####################################################

def safe_getattr(obj, name):
    log.debug('getattr(%s, %s)', obj, name)
    if name[0] == '_':
        raise AttributeError()
    return getattr(obj, name)

def safe_setattr(obj, name, value):
    log.debug("setattr(%s, %s, %s)", obj, name, value)
    setattr(obj, name, value)

def safe_getitem(self, item):
    log.debug("getitem(%s, %s)", self, item)
    return self[item]

def safe_write(self):
    log.debug("write(%s)", self)
    return self

###########################################################
#end DEBUG Restricted Access

class Script(object):
    def __init__(self, name, player, script_path=None):
        self.name = name
        self.player = player
        self.path = script_path
        self.version = 0
        self.lastHash = None
        self.source = None
        self.compiled = None
        self.module = None

    def __str__(self):
        return "Script["+self.name+"]("+")(".join([str(self.version), self.path])+")"

        
    def exists(self):
        return os.path.isfile(self.path)


    def load(self, path=None):
        # allow path override
        if not path:
            path = self.path
            
        with open(path) as f:
            m = hashlib.md5()
            self.source = ""
            for line in f:
                m.update(line)
                self.source += line

            # on success, update class data
            self.path = path
            h = m.digest()
            if h != self.lastHash:
                self.lastHash = h
                self.version += 1


    def save(self, data, path=None):
        if not path:
            path = self.path
        if not path:
            os.path.join(os.getcwd(), SCRIPT_DIR, self.player, self.name)

        # generate hash
        m = hashlib.md5()
        m.update(data)
        new_hash = m.digest()

        # don't update file if hashes match
        if new_hash == self.lastHash:
            self.path = path
            return False

        with open(path, "w") as f:
            f.write(data)

            # on success, update class data
            self.lastHash = m.digest()
            self.source = data
            self.path = path
            self.version += 1

        return True
    

    def compile(self):
        # Must have source to compile
        if not self.source:
            return False

        # Bare simple compilation, we'll need to add script
        # protections here
        self.compiled = compile_restricted(self.source, '<string>', 'exec')

        # Once script is compiled, we should not need the source
        #self.source = None
        #self.module = None
        # For now, keep source for debug purposes.

        return self.compiled != None

    def run(self, namespace):
        # Must have compiled script to continue
        if not self.compiled:
            self.compile()

        result = True
        try:
            exec(self.compiled) in namespace
        except Exception as err:
            result = False
            log.exception('%s: script (%s) generated an uncaught exception', self.name, self.path)
            raise
            
        return result
    
#end Script



class ScriptMgr(object):
    implements(IManager)
    
    def __init__(self):
        self.scripts = {}
        self.script_dir = os.path.join(os.getcwd(), SCRIPT_DIR)

        self.builtins = safe_builtins
        
    def loadAllScripts(self, script_dir=None):
        if script_dir:
            self.script_dir = os.path.join(os.getcwd(), script_dir)
            #sys.path.append(self.script_dir)
        
        if os.path.exists(self.script_dir):
            player_dirs = [f for f in os.listdir(self.script_dir)
                               if os.path.isdir(os.path.join(self.script_dir, f))]
        else:
            player_dirs = []
            
        for player_dir in player_dirs:
            count = self._loadPlayerScripts(player_dir, os.path.join(self.script_dir, player_dir))
            log.info("Loaded %d scripts for player %s", count, player_dir)

    def giveScript(self, player, name, data):
        # create new player if they don't exist yet
        player_scripts = self._getPlayerScripts(player, True)
        s = player_scripts.get(name)
        if s:
            recompile = s.save(data)
        else:
            script_path = os.path.join(self.script_dir, player, name + ".py")
            s = Script(name, player)
            recompile = s.save(data, script_path)
            log.info("Creating script %s", s.name)
            player_scripts[name] = s

        if recompile:
            log.info('Compiling script %s', s.name)
            s.compile()

    def getScript(self, player, name):
        player_scripts = self._getPlayerScripts(player)
        if player_scripts is None:
            log.info('Player "%s" does not exist', player)
            return None

        s = player_scripts.get(name)
        if not s:
            log.info('Script "%s" does not exist', name)
        return s

    def createNamespace(self, player):
        # use safe builtins
        ns = dict(__builtins__ = safe_builtins,
                  _print_ = PrintCollector,
                  __name__ = "__ship__",
                  _write_ = safe_write,
                  _getattr_ = safe_getattr, _setattr_ = safe_setattr,
                  _getitem_ = safe_getitem,
                  _getiter_ = lambda self: self.__iter__() )
        
        # add safe importor
        ps = self._getPlayerScripts(player)
        ns["__builtins__"]["__import__"] = player_import(player, ps)
        
        #print ns
        return ns

    def clear(self):
        self.scripts = {}

    def _getPlayerScripts(self, player, create_player = False):
        player_scripts = self.scripts.get(player)

        if not create_player:
            return player_scripts
        elif not player_scripts:
            # create new player
            log.info('Player "%s" does not exist, creating player', player)
            self.scripts[player] = {}

            # create the directory and for the new player
            player_path = os.path.join(self.script_dir, player)
            if not os.path.exists(player_path):
                log.info('Player directory "%s" does not exist, creating directory', player_path)
                os.makedirs(player_path)

        # return existing scripts or new player scripts
        return self.scripts[player]

    def _loadPlayerScripts(self, player, script_dir):
        player_scripts = self._getPlayerScripts(player, True)
        count = 0
        for script in glob.glob(os.path.join(script_dir,"*.py")):

            # add script, hash, and import
            log.info('Loading script: %s', script)
            name = os.path.splitext(os.path.basename(script))[0]
            s = player_scripts.get(name)
            if not s:
                s = Script(name, player)
                player_scripts[name] = s

            h = s.lastHash
            s.load(script)
            if not h == s.lastHash:
                s.compile()
                log.info('%s has changed [%s]', s, s.lastHash)
            else:
                log.debug("%s", s)
            count += 1
                
        log.debug(self.scripts[player])
        return count

#end ScriptMgr

def main(args=sys.argv[1:]):
    """Main entry point for pyship-script
    """
    log.info("Loading all scripts in %s", SCRIPT_DIR)
    sm = ScriptMgr()
    sm.loadAllScripts()


if __name__ == "__main__":
    sys.exit(main())
