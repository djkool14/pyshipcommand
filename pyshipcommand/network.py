
from zope.interface import Interface
from twisted.internet import reactor, defer
from twisted.cred.credentials import UsernamePassword
from twisted.spread import pb
from djlib.utils import enum

from logging import getLogger
log = getLogger('pyshipcommand.network')

class IPlayerCapable(Interface):

    def info():
        """Display info about the player."""

    def giveScript(name, data):
        """Transfer a script."""

    def addShip(pos_x, pos_y):
        """Create a ship for this player at position <pos_x, pos_y>"""

    def show(msg):
        """Print a message to the player."""
#end IPlayerCapable

class PBClient(object):

    # How long to wait for failed connection
    TIMEOUT = 10

    # Referenceable to handle server requests
    REF = None

    # Connection State
    ConnState = enum("DISCONNECTED",
                     "CONNECTING",
                     "CONNECTED",
                     "AUTHENTICATING",
                     "AUTHENTICATED")
    
    def __init__(self):
        self.state = self.ConnState.DISCONNECTED
        self._connector = None
        self.server = None

    def connect(self, addr, port):
        self._switchState(self.ConnState.CONNECTING)
        log.info("Connecting to Server %s:%d", addr, port)
        
        self._factory = pb.PBClientFactory()
        self._connector = reactor.connectTCP(addr, port, self._factory, self.TIMEOUT)
        self._switchState(self.ConnState.CONNECTED)

    def connected(self):
        log.info("Successfully Connected to Server %s:%d", addr, port)

    def isConnected(self):
        return self.state >= self.ConnState.CONNECTED

    def login(self, username, password):
        self._switchState(self.ConnState.AUTHENTICATING)
        self._defer = self._factory.login(UsernamePassword(username, password), self.REF)
        self._defer.addCallback(self._connected)
        self._defer.addErrback(self._error)

    def disconnect(self):
        self.server = None
        if self._connector:
            self._connector.disconnect()
            self._switchState(self.ConnState.DISCONNECTED)
        
    def _switchState(self, new_state):
        if new_state == self.state:
            return
        log.debug('Connection State changed to "%s"', self.ConnState.debug[new_state])
        self.state = new_state

    def _connected(self, perspective):
        self.server = perspective
        self._switchState(self.ConnState.AUTHENTICATED)
        self.connected()

    def _error(self, msg):
        log.warn(msg)
        
#end PBClient

# NET TYPES

from djlib.primitives import Vector
class NetVector(pb.Copyable, pb.RemoteCopy, Vector):

    @classmethod
    def fromVector(cls, vec):
        return NetVector(*vec.args())

    # serialize
    def getStateToCopy(self):
        d = self.__dict__.copy()
        d['v'] = list(self)
        return d

    # unserialize
    def setCopyableState(self, state):
        self.__init__(*state['v'])
        
    def __repr__(self):
        return "<"+", ".join(str(int(x)) for x in self)+">"
        
#end NetVector

class NetShip(pb.Copyable, pb.RemoteCopy):
    from math import atan2, degrees, pi
    
    def __init__(self, ship):
        self.id = ship.id
        self.pos = NetVector.fromVector(ship.pos)
        self.speed = ship.vel.length()
        self.rotation = -int(self.degrees(self.atan2(ship.heading.y, ship.heading.x)))
        self.end_rot = None
        self.start_pos = None
        self.end_pos = None
        self.recent_attack = ship.recent_attack
        self.weapon_a = 0.0
        self.mship = ship.isMothership()
        
    def update(self, nship):
        if self.id != nship.id:
            return
        
        self.speed = nship.speed

        if self.rotation != nship.rotation:
            self.end_rot = nship.rotation
            log.debug("Ship[%d] rotating: %d->%d", self.id, self.rotation, self.end_rot)

        self.recent_attack = nship.recent_attack
        if (self.recent_attack):
            self.weapon_a = 1.0
        
        if self.pos != nship.pos:
            self.start_pos = self.pos
            self.end_pos = nship.pos
            log.info("Ship[%d] set: %s->%s", self.id, self.start_pos, self.end_pos)
        elif self.start_pos:
            self.start_pos = None
            self.end_pos = None
            self.pos = nship.pos
            log.info("Ship[%d] cleared: %s", self.id, self.pos)
            
    def interpolate(self, dt):
        if self.start_pos:
            self.pos = self.start_pos.interpolate(self.end_pos, dt)

        if self.end_rot:
            # Must explicit handle 360 wrap
            d = (self.end_rot - self.rotation)
            d = d - 360 if d > 180 else d
            self.rotation += d*dt

            if dt > 1.0: # Stop lerp rotation after 1 second
                self.rotation = self.end_rot
                self.end_rot

        if self.weapon_a > 0.0:
            self.weapon_a -= dt
            if self.weapon_a < 0.0:
                self.weapon_a = 0.0

    # Needed for ExpandingRectTree
    def getPosition(self):
        return self.end_pos if self.end_pos else self.pos
        
#end NetShip

class NetInit(pb.Copyable, pb.RemoteCopy):

    def __init__(self):
        self.player_id = None
        self.player = None
        self.start_pos = None
        self.mship_id = None
        self.scripts = None

        self.server_step = None
        self.universe = None
        self.ships = None

#end NetInit

class NetScript(pb.Copyable, pb.RemoteCopy):

    def __init__(self, name, version, last_hash):
        self.name = name
        self.version = version
        self.last_hash = last_hash
        self.data = None

#end NetScript

import celestial
class NetCelestialBody(pb.Copyable, pb.RemoteCopy):

    def __init__(self, body):
        self.pos = NetVector.fromVector(body.pos)
        self.radius = body.radius
        self.type = -1
        for i, t in enumerate([celestial.Sun, celestial.Planet, celestial.Asteroid]):
            if isinstance(body, t):
                self.type = i
                break

    # Needed for ExpandingRectTree
    def getPosition(self):
        return self.pos
#end NetCelestialBody
